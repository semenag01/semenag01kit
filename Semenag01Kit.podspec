#
# Be sure to run `pod lib lint Semenag01Kit.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

#
# Be sure to run `pod lib lint Semenag01Kit.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|

    #root
        s.name      = 'Semenag01Kit'
        s.version   = '3.0.0'
        s.summary   = 'Semenag01Kit descriptions'
        s.license  = 'MIT'

        s.homepage  = 'https://bitbucket.org/semenag01'
        s.authors   = {'semenag01' => 'semenag01@meta.ua'}
        s.source    = { :git => 'https://semenag01@bitbucket.org/semenag01/semenag01kit.git', :branch => 'master', :tag => '3.0.0' }

    #platform
        s.platform = :ios
        s.ios.deployment_target = '9.0'

    #build settings
        s.requires_arc = true

    #file patterns
        s.source_files = 'Semenag01Kit/Semenag01Kit.h'

        s.resources    = 'Semenag01Kit/Resources/semenag01Kit.bundle'

        s.frameworks   = 'QuartzCore'
        s.frameworks   = 'CoreData'

        s.source_files = 'Semenag01Kit/**/*.{h,m}'

        s.dependency 'MBProgressHUD'
        s.dependency 'AFNetworking', '~> 4.0'
end
