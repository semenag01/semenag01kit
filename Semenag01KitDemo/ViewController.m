//
//  ViewController.m
//  semenag01Kit
//
//  Created by semenag01 on 5/12/15.
//  Copyright (c) 2015 semenag01. All rights reserved.
//

#import "ViewController.h"
#import "SMAnimator.h"
#import <AVFoundation/AVFoundation.h>
#import "SMKitDefines.h"
#import <AVFoundation/AVFoundation.h>
#import "SMKeyboardAvoidingScrollView.h"
#import "SMTextField.h"
#import "SMGateway.h"
#import "SMPopupDatePicker.h"
#import "SMAlertView.h"

@interface ViewController ()
{
    SMPopupDatePicker *picker;
}

@property (strong, nonatomic) IBOutlet UIView *vTest;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    picker = [[SMPopupDatePicker alloc] init];
    
    [self.scrollView addObjectForKeyboard:self.tfTest0];
    [self.scrollView addObjectForKeyboard:self.tfTest1];
    [self.scrollView addObjectForKeyboard:self.tfTest2];
    [self.scrollView addObjectForKeyboard:self.tfTest3];
    [self.scrollView addObjectForKeyboard:self.tfTest4];
    
    self.scrollView.showsKeyboardToolbar = YES;
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)didBtStopClicked:(id)sender
{
}

- (IBAction)didBtTestClicked:(UIButton *)sender
{
    [picker prepareToShow];
    
    SMAlertItem *item = [SMAlertItem makeWithTitle:@"itemGrren" color:[UIColor greenColor] style:UIAlertActionStyleCancel];
    [self showAlertViewWithTitle:@"TEST" message:@"BODY" cancelButtonTitle:nil otherButtonTitles:@[item, @"test"] dismissBlock:^(id alertController, NSInteger buttonIndex) {
        
    }];
//    if (SM_IS_IPAD)
//    {
//        [picker showFromRect:sender.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
//    } else
//    {
//        [picker showWithAnimation:YES inView:self.view];
//    }
    
//    self.vTest.center = CGPointMake(200, 100);
//
//    SMAnimatorViewCenter *animation1 = [SMAnimatorViewCenter makeWithView:self.vTest duration:1.0 comlition:^(SMAnimator *aAnimator, BOOL aCanceled) {
//        NSLog(@"Finish %@",NSStringFromClass([aAnimator class]));
//    }];
//    animation1.center = CGPointMake(200, 500);
//
//    
//    SMAnimatorViewAlpha *animation2 = [SMAnimatorViewAlpha makeWithView:self.vTest duration:1.0 comlition:^(SMAnimator *aAnimator, BOOL aCanceled) {
//        NSLog(@"Finish %@",NSStringFromClass([aAnimator class]));
//    }];
//    animation2.alpha = 0;
//    
//
//    SMAnimatorGroup *abGroup = [SMAnimatorGroup makeWithAniamtions:@[animation1,animation2]];
//    abGroup.repeatCount = 2;
//    SMAnimatorViewBackgroundColor *abg = [SMAnimatorViewBackgroundColor makeWithView:self.vTest duration:2 comlition:nil];
//    abg.repeatCount = 2;
//    abg.backgroundColor = [UIColor redColor];
//    
//    SMAnimatorSequence *as = [SMAnimatorSequence makeWithAniamtions:@[abGroup,abg]];
//    [as start];
//    anBack.repeatCount = 5;
//    SMAnimatorCompound *anSeq = [SMAnimatorSequence makeWithAniamtions:@[animation1]];
//    anSeq.repeatForever = NO;
//    anSeq.repeatCount = 3;
//    SMAnimatorGroup *anGr = [SMAnimatorGroup makeWithAniamtions:@[anSeq,animation2]];
//    anGr.repeatForever = YES;
//    anSeq.delay = 2;
//    animation.center = CGPointMake(0, 400);
}

@end
