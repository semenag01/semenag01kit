//
//  AppDelegate.h
//  semenag01Kit
//
//  Created by semenag01 on 5/12/15.
//  Copyright (c) 2015 semenag01. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

