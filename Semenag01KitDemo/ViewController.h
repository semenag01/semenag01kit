//
//  ViewController.h
//  semenag01Kit
//
//  Created by semenag01 on 5/12/15.
//  Copyright (c) 2015 semenag01. All rights reserved.
//

#import "SMViewController.h"

@class SMTextField,SMKeyboardAvoidingScrollView;

@interface ViewController : SMViewController

@property (weak, nonatomic) IBOutlet SMTextField *tfTest0;
@property (weak, nonatomic) IBOutlet SMTextField *tfTest1;
@property (weak, nonatomic) IBOutlet SMTextField *tfTest2;
@property (weak, nonatomic) IBOutlet SMTextField *tfTest3;
@property (weak, nonatomic) IBOutlet SMTextField *tfTest4;
@property (weak, nonatomic) IBOutlet SMKeyboardAvoidingScrollView *scrollView;

@end

