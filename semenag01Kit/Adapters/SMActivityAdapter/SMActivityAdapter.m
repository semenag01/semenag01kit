//
//  SMActivityAdapter.m
//  Semenag01Kit
//
//  Created by semenag01 on 15.04.13.
//  Copyright (c) 2013 semenag01. all rights reserved.
//

#import "SMActivityAdapter.h"

@implementation SMActivityAdapter

@dynamic activity;

- (id)activity
{
    // override it in subclasses
    return nil;
}

- (void)configureWithView:(UIView*)aView
{
    // override it in subclasses
}

- (void)showActivity:(BOOL)animated
{
    // override it in subclasses
}

- (void)hideActivity:(BOOL)animated
{
    // override it in subclasses
}

- (void)releaseActivityView
{
    // override it in subclasses
}

@end
