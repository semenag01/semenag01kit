//
//  SMActivityHUD.h
//  Semenag01Kit
//
//  Created by semenag01 on 15.04.13.
//  Copyright (c) 2013 semenag01. all rights reserved.
//

#import "SMActivityAdapter.h"

/**
 * This adapter provide wrapper around MBProgressHUD activity.
 * @see SMActivityAdapter
 */

@class MBProgressHUD;

@interface SMActivityHUD : SMActivityAdapter
{
    MBProgressHUD *activity;
}

@end
