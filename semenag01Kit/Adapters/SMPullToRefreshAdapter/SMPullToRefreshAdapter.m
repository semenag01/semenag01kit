//
//  SMPullToRefreshAdapter.m
//  Semenag01Kit
//
//  Created by semenag01 on 15.04.13.
//  Copyright (c) 2013 semenag01. all rights reserved.
//

#import "SMPullToRefreshAdapter.h"

@implementation SMPullToRefreshAdapter

- (void)configureWithScrollView:(UIScrollView *)aScrollView
{
    // override it in subclasses
}

- (void)releaseRefreshControl
{
    // override it in subclasses
}

- (void)beginPullToRefresh
{
    self.refreshCallback(self);
}

- (void)endPullToRefresh
{
    // override it in subclasses
}

@end
