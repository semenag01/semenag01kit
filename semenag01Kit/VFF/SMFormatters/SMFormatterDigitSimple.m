//
//  SMFormatterDigitSimple.m
//  Semenag01Kit
//
//  Created by semenag01 on 12/14/11.
//  Copyright (c) 2011 semenag01. all rights reserved.
//

#import "SMFormatterDigitSimple.h"

@interface SMFormatterDigitSimple ()

- (BOOL)formatForTextField:(UITextField *)textField
  shouldChangeCharactersInRange:(NSRange)range
              replacementString:(NSString *)string;

/*
 Attemps to format the phone number to the specified locale.
 */
- (NSString *)format:(NSString *)aText;

/*
 Strips the input string from characters added by the formatter.
 Namely, it removes any character that couldn't have been entered by the user.
 */
- (NSString *)strip:(NSString *)aText;

/*
 Returns true if the character comes from a phone pad.
 */
- (BOOL)canBeInputByPhonePad:(unichar)c;

@end

@implementation SMFormatterDigitSimple

- (id)initWithFormats:(NSArray *)aFormats
{
    self = [super init];
    if (self)
    {
        formats = aFormats;
        
        acceptableInputCharacters = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    }
    return self;
}

- (BOOL)formatWithNewCharactersInRange:(NSRange)aRange replacementString:(NSString*)aString
{
    UITextField* textField = (UITextField*)self.formattableObject;
    return [self formatForTextField:textField shouldChangeCharactersInRange:aRange replacementString:aString];
}

- (BOOL)formatForTextField:(UITextField *)textField
  shouldChangeCharactersInRange:(NSRange)range
              replacementString:(NSString *)string
{
    BOOL result = NO;
    if ([string length] > 0)
    {
        if ([string rangeOfCharacterFromSet:acceptableInputCharacters].location != NSNotFound)
        {
            NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
            text = [self format:text];
            if (text)
                textField.text = text;
        }
    }
    else
    {
        NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
        text = [self format:text];
        if (text)
            textField.text = text;
    }
    
    return result;
}

- (NSString *)formattedTextFromString:(NSString *)aOriginalString
{
    NSString *result = [self format:aOriginalString];
    if (result)
        return result;
    
    return aOriginalString; // cant format input string
}

- (NSString *)format:(NSString *)ipAddress
{
    NSString *input = [self strip:ipAddress];
    for(NSString *format in formats)
    {
        int i = 0;
        NSMutableString *temp = [NSMutableString new];
        
        for(int p = 0; temp != nil && i < [input length] && p < [format length]; p++)
        {
            unichar c = [format characterAtIndex:p];
            BOOL required = [self canBeInputByPhonePad:c];
            unichar next = [input characterAtIndex:i];
            
            switch(c)
            {
                case '#':
                    if(next < '0' || next > '9')
                    {
                        temp = nil;
                        break;
                    }
                    [temp appendFormat:@"%c", next]; i++;
                    break;
                default:
                    if(required)
                    {
                        if(next != c)
                        {
                            temp = nil;
                            break;
                        }
                        [temp appendFormat:@"%c", next]; i++;
                        
                    }
                    else
                    {
                        [temp appendFormat:@"%c", c];
                        if(next == c) i++;
                    }
                    break;
            }
        }
        if(i == [input length])
        {
            return temp;
        }
    }
    
    if (self.acceptsNotPredefinedFormatter)
        return input;
    else
        return nil;
}

- (NSString *)strip:(NSString *)aText
{
    NSMutableString *res = [NSMutableString new];
    for(int i = 0; i < [aText length]; i++)
    {
        unichar next = [aText characterAtIndex:i];
        if([self canBeInputByPhonePad:next])
            [res appendFormat:@"%c", next];
    }
    return res;
}

- (BOOL)canBeInputByPhonePad:(unichar)c
{
    return [acceptableInputCharacters characterIsMember:c];
}

- (NSString*)rawText
{
    return (self.formattableObject.formattingText) ? ([self strip:self.formattableObject.formattingText]) : (nil);
}

@end
