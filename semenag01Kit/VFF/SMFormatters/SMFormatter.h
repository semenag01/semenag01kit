//
//  SMFormatter.h
//  Semenag01Kit
//
//  Created by semenag01 on 22.04.13.
//
//

#import <Foundation/Foundation.h>

@class SMFormatter;

@protocol SMFormatterProtocol <NSObject>

@property (nonatomic) SMFormatter *formatter;
@property (nonatomic) NSString *formattingText;

@end


@interface SMFormatter : NSObject

@property (nonatomic, weak) id<SMFormatterProtocol> formattableObject;

- (NSString*)formattedTextFromString:(NSString*)aOriginalString;
- (BOOL)formatWithNewCharactersInRange:(NSRange)aRange replacementString:(NSString*)aString;

@property (nonatomic) NSString *rawText;

@end

