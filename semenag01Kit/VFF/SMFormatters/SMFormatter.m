//
//  SMFormatter.m
//  Semenag01Kit
//
//  Created by semenag01 on 22.04.13.
//
//

#import "SMFormatter.h"

@implementation SMFormatter

- (NSString *)formattedTextFromString:(NSString *)aOriginalString
{
    return aOriginalString;
}

- (BOOL)formatWithNewCharactersInRange:(NSRange)aRange replacementString:(NSString*)aString
{
    return YES;
}

- (NSString*)rawText
{
    return nil;
}

- (void)setRawText:(NSString *)rawText
{
    self.formattableObject.formattingText = [self formattedTextFromString:rawText];
}

@end
