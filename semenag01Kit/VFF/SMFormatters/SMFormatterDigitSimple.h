//
//  SMFormatterDigitSimple.h
//  Semenag01Kit
//
//  Created by semenag01 on 12/14/11.
//  Copyright (c) 2011 semenag01. all rights reserved.
//

#import "SMFormatter.h"

@interface SMFormatterDigitSimple : SMFormatter
{
    NSArray *formats;

    NSCharacterSet *acceptableInputCharacters;
}

- (id)initWithFormats:(NSArray *)aFormats;

/*
 * if result string after formatting is not suitable for any format of current local then return just input string
 * if value is NO then return prev formatted string before last input
 * by default is NO
 */
@property (nonatomic, assign) BOOL acceptsNotPredefinedFormatter;

@end
