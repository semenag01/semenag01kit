//
//  SMValidationGroup.h
//  Semenag01Kit
//
//  Created by semenag01 on 10.07.11.
//  Copyright 2011 semenag01. all rights reserved.
//

#import <Foundation/Foundation.h>
#import "SMValidator.h"

@class SMValidationGroup;
@protocol SMValidationGroupProtocol <NSObject>

- (void)applyValideStateForGroup:(SMValidationGroup *)aGroup;
- (void)applyInvalideStateForGroup:(SMValidationGroup *)aGroup;
@end

@protocol SMValidationGroupDelegate;

@interface SMValidationGroup : NSObject
{
    NSMutableArray* validators;
    UIImage* invalidIndicatorImage;
}

@property (nonatomic, retain) UIImage* invalidIndicatorImage;
@property (nonatomic, weak) NSObject<SMValidationGroupDelegate>* delegate;

- (instancetype)initWithValidators:(NSArray <SMValidator *>*)aValidators;

- (void)addValidator:(SMValidator*)aValidator;
- (void)addValidators:(NSArray*)aValidators;
- (void)removeAllValidators;

- (NSArray <id <SMValidationProtocol>> *)validate;
- (void)hideInvalidIndicators;

- (void)showInvalidViewForField:(UITextField*)aTextField;
- (void)hideInvalidViewForField:(UITextField*)aTextField;

- (void)applyShakeForWrongFieldsIfCan;
- (void)refreshStatesInFields;

@end


@protocol SMValidationGroupDelegate <NSObject>
@optional
- (void)proccessValidationResults:(NSMutableArray*)aValidationResults;
- (void)prepareInvalidIndicatorView:(UITextField *)aTextField;
@end

