//
//  UIImage+fixOrientation.h
//  Vivaturizmo-iOS6
//
//  Created by semenag01 on 17.10.12.
//  Copyright (c) 2012 semenag01. all rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (fixOrientation)

- (UIImage *)fixOrientation;
- (UIImage *)fixOrientationDevice;

@end
