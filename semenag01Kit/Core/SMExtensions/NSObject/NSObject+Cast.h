//
//  NSObject+Cast.h
//  RyppleIOSApp
//
//  Created by semenag01 on 9/25/15.
//  Copyright © 2015 semenag01. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Cast)

+ (instancetype)asType:(id)object;

@end
