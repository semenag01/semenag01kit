//
//  NSData+Encryption.h
//  Semenag01Kit
//
//  Created by semenag01 on 11.07.13.
//  Copyright (c) 2013 semenag01. all rights reserved.
//

@interface NSData (Encryption)

- (NSData *)md5Digest;
- (NSData *)sha1Digest;

- (NSString *)hexStringValue;

- (NSString *)base64Encoded;
- (NSData *)base64Decoded;

@end
