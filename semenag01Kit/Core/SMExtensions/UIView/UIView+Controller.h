//
//  UIView+Controler.h
//  Semenag01Kit
//
//  Created by semenag01 on 11.07.13.
//  Copyright (c) 2013 semenag01. all rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Controller)

- (UIViewController*)firstAvailableUIViewController;
- (id)traverseResponderChainForUIViewController;

@end
