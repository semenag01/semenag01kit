//
//  UIImageView+More.h
//  BondJeansApp
//
//  Created by semenag01 on 5/2/15.
//  Copyright (c) 2015 semenag01. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (More)

- (void)setImage:(UIImage *)image animte:(BOOL)animate;

@end
