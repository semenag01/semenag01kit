//
//  UIColor+Color.h
//  Semenag01Kit
//
//  Created by semenag01 on 11.07.13.
//  Copyright (c) 2013 semenag01. all rights reserved.
//

#import <UIKit/UIKit.h>

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define UIColorFromARGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:((float)((rgbValue & 0xFF000000) >> 24))/255.0]
#define UIColorFromRGBA(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF000000) >> 24))/255.0 green:((float)((rgbValue & 0xFF0000) >> 16))/255.0 blue:((float)((rgbValue & 0xFF00) >> 8 ))/255.0 alpha:((float)((rgbValue & 0xFF))/255.0)]

@interface UIColor (Color)

+ (UIColor*)colorWithRedI:(NSInteger)red
                   greenI:(NSInteger)green
                    blueI:(NSInteger)blue
                   alphaI:(NSInteger)alpha;

- (NSString *)htmlHexString;
- (CGFloat)red;
- (CGFloat)green;
- (CGFloat)blue;

+ (UIColor *)colorWithHexString:(NSString *)hexString;

@end
