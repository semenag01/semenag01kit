//
//  SMPair.h
//  Semenag01Kit
//
//  Created by semenag01 on 10/13/11.
//  Copyright 2011 semenag01. all rights reserved.
//

#import <Foundation/Foundation.h>

@interface SMPair <TypeFirst, TypeSecond> : NSObject

@property (nonatomic, strong) TypeFirst first;
@property (nonatomic, strong) TypeSecond second;

+ (instancetype)pairWithFirst:TypeFirstaFirst second:(TypeSecond)aSecond;
- (instancetype)initWithFirst:(TypeFirst)aFirst second:(TypeSecond)aSecond;

@end


@interface NSArray (SMPair)

- (SMPair*)pairByFirst:(id)aFirst;
- (SMPair*)pairBySecond:(id)aSecond;

@end

