//
//  SMHelper.h
//  Pro-Otdyh
//
//  Created by semenag01 on 24.03.12.
//  Copyright (c) 2012 semenag01. all rights reserved.
//

#import <Foundation/Foundation.h>
#import "SMExtensions.h"

inline UIViewController* SMGetPrimeViewController(void);

typedef BOOL (^SMDividedComparator)(id anObj1, id anObj2);
NSMutableArray* SMDivideArray(NSArray* aDividedArray, NSString* aFieldName, BOOL anAscending, SMDividedComparator aComparator);

NSDate* SMDateFromTimeStampInDictionary(NSDictionary* aDictionary, NSString* aKey);

inline NSString* SMDocumentDirectoryPath(void);
inline NSString* SMCacheDirectoryPath(void);
inline NSString* SMGenerateUUID(void);

#define SMFrameFromSize(size) CGRectMake(0, 0, size.width, size.height);
