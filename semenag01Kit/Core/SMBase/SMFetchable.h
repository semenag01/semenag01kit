//
//  SMFetchable.h
//  Semenag01Kit
//
//  Created by semenag01 on 10/14/11.
//  Copyright 2011 semenag01. all rights reserved.
//

#import <Foundation/Foundation.h>

//==============================================================================
@protocol SMFetchable <NSObject>

/**
 * Need fetch data from server
 **/
@property (nonatomic, assign) BOOL needFetch;

- (void)fetchData;

@end
