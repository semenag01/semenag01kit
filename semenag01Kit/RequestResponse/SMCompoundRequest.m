//
//  SMCompoundRequest.m
//  Semenag01Kit
//
//  Created by OLEKSANDR SEMENIUK on 9/29/17.
//  Copyright © 2017 OLEKSANDR SEMENIUK. All rights reserved.
//

#import "SMCompoundRequest.h"

@implementation SMCompoundRequest
@synthesize requests,currentExecutingIndex;

- (instancetype)initWithRequests:(NSArray <SMRequest *>*)aRequests
{
    NSAssert([aRequests count], @"SMCompoundRequest: count of requests should be more than 0");

    self = [super init];
    
    if (self)
    {
        requests = aRequests;
    }
    
    return self;
}

- (BOOL)canExecute
{
    BOOL result = YES;
    
    for (SMRequest *r in requests)
    {
        if (!r.canExecute)
        {
            result = NO;
            break;
        }
    }
    
    return result;
}

- (void)start
{
    cancelled = NO;
    executing = YES;
    finished = NO;
    
    if (self.executeRequestingParallel)
    {
        [self startParallel];
    } else
    {
        [self startSequence];
    }
}

- (void)startParallel
{
    dispatch_group_t requestGroup = dispatch_group_create();
    
    __block NSMutableArray <SMResponse *> *responses = [NSMutableArray arrayWithCapacity:self.requests.count];
    
    for (NSInteger i = 0; i < self.requests.count; i++)
    {
        SMRequest *request = self.requests[i];
        
        dispatch_group_enter(requestGroup);
        
        [request startWithResponseBlockInMainQueue:^(SMResponse *aResponse)
         {
             responses[i] = aResponse;
             dispatch_group_leave(requestGroup);
         }];
    }
    
    SMWeakSelf
    dispatch_group_notify(requestGroup, dispatch_get_main_queue(), ^{
        [weakSelf finishedAllRequestsWithResponses:responses];
    });
}

- (void)startSequence
{
    [self startRequestWithIndex:0];

    __block NSMutableArray <SMResponse *> *responses = [NSMutableArray arrayWithCapacity:self.requests.count];

    SMWeakSelf;
    for (NSUInteger i = 0; i < self.requests.count; i++)
    {
        [requests[i] addResponseBlock:^(SMResponse *aResponse)
         {
             responses[i] = aResponse;
             
             void(^finishBlock)(void) = ^(void)
             {
                 [weakSelf finishedAllRequestsWithResponses:responses];
             };
             
             if (aResponse.success)
             {
                 if (i < weakSelf.requests.count - 1)
                 {
                     [weakSelf startRequestWithIndex:i+1];
                 } else
                 {
                     finishBlock();
                 }
             } else
             {
                 if (weakSelf.continueRequestIfAtLeastFail)
                 {
                     if (i < weakSelf.requests.count - 1)
                     {
                         [weakSelf startRequestWithIndex:i+1];
                     } else
                     {
                         finishBlock();
                     }
                 } else
                 {
                     finishBlock();
                 }
             }
         } responseQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)];
    }
}

- (void)finishedAllRequestsWithResponses:(NSMutableArray <SMResponse *>*)aResponses
{
    SMResponse *result = [SMResponse new];
    result.success = YES;
    
    if (self.putResponsesToOneResult)
    {
        result.boArray = aResponses;
    } else
    {
        for (SMResponse *response in aResponses)
        {
            if (!response.success && result.success)
            {
                result.success = response.success;
                result.error = response.error;
                result.textTitle = response.textTitle;
                result.textMessage = response.textMessage;
            }
            
            [result.boArray addObject:response.boArray];
            [result.dataDictionary addEntriesFromDictionary:response.dataDictionary];
        }
    }
    
    executing = NO;
    finished = YES;

    [self executeAllResponseBlocksWithResponse:result];
}

- (BOOL)isExecuting
{
    return executing;
}

- (void)cancel
{
    cancelled = YES;
    executing = NO;
    finished = YES;
    
    [requests[currentExecutingIndex] cancel];
}

- (BOOL)isCancelled
{
    return cancelled;
}

- (BOOL)isFinished
{
    return finished;
}

- (void)startRequestWithIndex:(NSUInteger)aIndex
{
    currentExecutingIndex = aIndex;
    [requests[currentExecutingIndex] start];
}

@end
