//
//  SMResponse.h
//  Semenag01Kit
//
//  Created by semenag01 on 15.04.13.
//  Copyright (c) 2013 semenag01. all rights reserved.
//

#import <Foundation/Foundation.h>

@interface SMResponse : NSObject

@property (nonatomic, assign) BOOL success;

@property (nonatomic, assign) NSInteger code;
@property (nonatomic, strong) NSString *textMessage;
@property (nonatomic, strong) NSString *textTitle;

@property(nonatomic, strong) id cursorNext;

@property (nonatomic, readonly) NSMutableDictionary *dataDictionary;
@property (nonatomic, strong) NSMutableArray *boArray;

@property (nonatomic, strong) NSError *error;
@property (nonatomic, assign, getter = isRequestCancelled) BOOL requestCancelled;

@end
