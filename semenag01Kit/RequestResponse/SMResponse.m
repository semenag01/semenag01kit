//
//  SMResponse.m
//  Semenag01Kit
//
//  Created by semenag01 on 15.04.13.
//  Copyright (c) 2013 semenag01. all rights reserved.
//

#import "SMResponse.h"

@implementation SMResponse

- (instancetype)init
{
    self = [super init];
    if(self)
    {
        _dataDictionary = [NSMutableDictionary new];
        _boArray = [NSMutableArray new];
    }
    return self;
}

@end
