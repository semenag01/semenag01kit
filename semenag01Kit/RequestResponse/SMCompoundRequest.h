//
//  SMCompoundRequest.h
//  Semenag01Kit
//
//  Created by OLEKSANDR SEMENIUK on 9/29/17.
//  Copyright © 2017 OLEKSANDR SEMENIUK. All rights reserved.
//

#import "SMRequest.h"

@interface SMCompoundRequest : SMRequest
{
    NSArray <SMRequest *>*requests;
    
    BOOL cancelled;
    BOOL finished;
    BOOL executing;
    NSUInteger currentExecutingIndex;
}

- (instancetype)initWithRequests:(NSArray <SMRequest *>*)aRequests;

@property (nonatomic, readonly) NSArray <SMRequest *>*requests;

@property (nonatomic, assign) BOOL executeRequestingParallel;// Default NO
@property (nonatomic, assign) BOOL putResponsesToOneResult;// Default NO

// Only for executeRequestingParallel = NO
@property (nonatomic, assign) BOOL continueRequestIfAtLeastFail;
@property (nonatomic, readonly) NSUInteger currentExecutingIndex;

@end
