//
//  NSArray+DBStorage.h
//  Semenag01Kit
//
//  Created by OLEKSANDR SEMENIUK on 3/3/18.
//  Copyright © 2018 Developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SMDBStorableObject.h"

@interface NSArray (DBStorage)

- (NSMutableArray <NSManagedObject <SMDBStorableObject>*>*)moveToContext:(NSManagedObjectContext *)context;

@end

