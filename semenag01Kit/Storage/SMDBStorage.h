//
//  SMStorage.h
//  Semenag01Kit
//
//  Created by Developer on 1/18/18.
//  Copyright © 2018 Developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "SMDBStorableObject.h"
#import "NSArray+DBStorage.h"
typedef void (^SMStorageContextBlock)(NSManagedObjectContext *aContext);


typedef enum SMClonePolicy
{
    SMClonePolicyAsOriginal,
    SMClonePolicyAsTemp,
    SMClonePolicyInsertIntoDefaultContext
} SMClonePolicy;

typedef enum SMStorageSaveType
{
    SMStorageSaveTypeMultiContexts,
    SMStorageSaveTypeSingleContext
} SMStorageSaveType;


@interface SMDBStorage : NSObject


/**
 * Use only in main queue
 **/
@property (nonatomic, strong, readonly) NSManagedObjectContext *defaultContext;

#pragma mark - Configuration
@property (nonatomic, readonly) NSString *persistentStoreName;
@property (nonatomic, assign) BOOL shouldCacheStorage;
@property (nonatomic, readonly) NSDictionary *migrationPolicy;
@property (nonatomic, readonly) NSString *storeType;
@property (nonatomic, assign, readonly) BOOL mergeModels;

@property (nonatomic, assign) SMStorageSaveType saveType;

#pragma mark - Save
- (void)saveWithBlock:(SMStorageContextBlock)block completion:(void(^)(void))completion;
- (void)saveWithBlockAndWait:(SMStorageContextBlock)block completion:(void(^)(void))completion;
- (void)saveWithBlockAndWait:(SMStorageContextBlock)block;

#pragma mark - Perform blocks
- (void)defaultContextBlockAndWait:(SMStorageContextBlock)aBlock;
- (void)defaultContextBlock:(SMStorageContextBlock)aBlock;

#pragma mark - Remove entities
- (void)removeAllEntitiesWithName:(NSString *)anEntityName;

#pragma mark - Clear
- (void)clear;
- (void)clearAndWait;

#pragma mark - Remove Sync
- (void)removeObject:(NSManagedObject <SMDBStorableObject>*)aObject;
- (void)removeObjects:(NSArray <NSManagedObject <SMDBStorableObject>*>*)aObjects;

#pragma mark - Clone
- (NSManagedObject *)cloneObject:(NSManagedObject *)anObject clonePolicy:(SMClonePolicy)aClonePoliy;

@end

