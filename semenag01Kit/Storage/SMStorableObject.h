//
//  SMStorableObject.h
//  Semenag01Kit
//
//  Created by semenag01 on 29.04.13.
//  Copyright (c) 2013 semenag01. all rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

__attribute__ ((deprecated("Use SMDBStorableObject instead.")))
@protocol SMStorableObject <NSObject>

+ (NSManagedObjectID*)isStorageContainedObject:(NSManagedObject*)anObject;

- (void)setupWithDictionary:(NSDictionary*)aData;

/**
 * Insert into context without immediate save into storage
 **/
- (void)insertIntoContext;

@optional

+ (NSArray*)defaultSortDescriptors;

/**
 * Determine not updatable attributes in model during update (see insertModelsWithUpdate:modelAttributesToUpdate: in SMStorable)
 **/
+ (NSSet*)notUpdatableAttributes;

@end
