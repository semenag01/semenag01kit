//
//  SMDBRequest.h
//  Semenag01Kit
//
//  Created by Developer on 1/18/18.
//  Copyright © 2018 Developer. All rights reserved.

#import "SMRequest.h"
#import "SMDBStorage.h"

@interface SMDBRequest : SMRequest
{
    SMDBStorage *storage;

    BOOL cancelled;
    BOOL executing;
}

@property (nonatomic, strong) NSFetchRequest *fetchRequest;

- (instancetype)initWithStorage:(SMDBStorage *)aStorage;

@end
