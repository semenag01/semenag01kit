//
//  SMDBRequest.m
//  Semenag01Kit
//
//  Created by Developer on 1/18/18.
//  Copyright © 2018 Developer. All rights reserved.

#import "SMDBRequest.h"

@interface SMDBRequest ()

- (SMResponse *)executeRequest:(NSFetchRequest *)aRequest
                     inContext:(NSManagedObjectContext *)aContext;

@end

@implementation SMDBRequest

- (instancetype)initWithStorage:(SMDBStorage *)aStorage
{
    self = [super init];
    
    if(self)
    {
        storage = aStorage;
    }
    
    return self;
}


#pragma mark - Request execute

- (BOOL)canExecute
{
    return storage && self.fetchRequest;
}

- (void)start
{
    cancelled = NO;
    executing = YES;
    
    SMWeakSelf
    [storage defaultContextBlock:^(NSManagedObjectContext *aContext) {
        SMStrongSelf
        if (strongSelf)
        {
            SMResponse *response = nil;
            if([weakSelf isCancelled])
            {
                strongSelf->executing = NO;
                
                response = [SMResponse new];
                response.requestCancelled = YES;
                response.success = NO;
                
                if(weakSelf.executeAllResponseBlocksSync)
                {
                    [weakSelf executeSynchronouslyAllResponseBlocksWithResponse:response];
                }
                else
                {
                    [weakSelf executeAllResponseBlocksWithResponse:response];
                }
            } else
            {
                response = [strongSelf executeRequest:strongSelf.fetchRequest inContext:aContext];
                
                strongSelf->executing = NO;
                if(weakSelf.executeAllResponseBlocksSync)
                {
                    [weakSelf executeSynchronouslyAllResponseBlocksWithResponse:response];
                }
                else
                {
                    [weakSelf executeAllResponseBlocksWithResponse:response];
                }
            }
        }
    }];
}


#pragma mark -

- (SMResponse *)executeRequest:(NSFetchRequest *)aRequest inContext:(NSManagedObjectContext *)aContext
{
    NSError *error = nil;
    NSArray *results = [aContext executeFetchRequest:aRequest error:&error];
    
    SMResponse *response = [SMResponse new];
    if([results count])
    {
        [response.boArray addObjectsFromArray:results];
    }
    
    response.error = error;
    response.code = error.code;
    response.requestCancelled = [self isCancelled];
    response.success = !response.error && !response.requestCancelled;

    return response;
}

- (void)cancel
{
    cancelled = YES;
}

- (BOOL)isExecuting
{
    return executing;
}

- (BOOL)isCancelled
{
    return cancelled;
}

@end
