//
//  SMStorage.m
//  Semenag01Kit
//
//  Created by Developer on 1/18/18.
//  Copyright © 2018 Developer. All rights reserved.
//

#import "SMDBStorage.h"
#import "SMKitDefines.h"
#import "SMPair.h"
#import <objc/runtime.h>


@interface SMDBStorage ()

@property (nonatomic, strong, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, strong, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (nonatomic, strong, readonly) NSManagedObjectContext *savingContext;

@end

@implementation SMDBStorage

@dynamic migrationPolicy;
@dynamic storeType;
@dynamic persistentStoreName;
@dynamic mergeModels;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize defaultContext = _defaultContext;

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        _defaultContext = [self defaultContext];
    }
    
    return self;
}


#pragma mark - Configuration

- (NSString *)persistentStoreName
{
    NSAssert(NO, @"Override this!!!");
    return nil;
}

- (NSDictionary *)migrationPolicy
{
    return
    @{
      NSMigratePersistentStoresAutomaticallyOption : @(YES),
      NSInferMappingModelAutomaticallyOption : @(YES)
      };
}

- (NSString *)storeType
{
    return NSSQLiteStoreType;
}

- (BOOL)mergeModels
{
    return YES;
}


#pragma mark - CoreData preparation

- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel == nil)
    {
        if (![self mergeModels])
        {
            NSString *name = [self.persistentStoreName stringByReplacingOccurrencesOfString:@".sqlite" withString:@""];
            NSString *modelPath = [[NSBundle mainBundle] pathForResource:name ofType:@"momd"];
            NSURL *modelURL = [NSURL fileURLWithPath:modelPath];
            _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
        }
        else
        {
            _managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:[NSArray arrayWithObject:[NSBundle mainBundle]]];
        }
    }
    
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator == nil)
    {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSURL *directoryURL =  [[fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
        NSURL *storeUrl = [NSURL URLWithString:self.persistentStoreName relativeToURL:directoryURL];
        [storeUrl setResourceValue:@(!self.shouldCacheStorage) forKey:NSURLIsExcludedFromBackupKey error:nil];
        
        _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.managedObjectModel];
        
        NSError *error = nil;
        NSPersistentStore *store = [_persistentStoreCoordinator addPersistentStoreWithType:self.storeType
                                                                             configuration:nil
                                                                                       URL:storeUrl
                                                                                   options:self.migrationPolicy
                                                                                     error:&error];
        
        if (!store)
        {
            NSLog(@"MUStorage: Unresolved error %@", error);
# if DEBUG
            abort();
#endif
        }
    }
    
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)savingContext
{
    NSManagedObjectContext *result;
    
    switch (self.saveType)
    {
        case SMStorageSaveTypeMultiContexts:
        {
            result = [self contextWithParent:self.defaultContext];
        }
            break;
            
        case SMStorageSaveTypeSingleContext:
        {
            static NSManagedObjectContext *savingContext;
            
            static dispatch_once_t onceToken;
            
            dispatch_once(&onceToken, ^{
                savingContext = [self contextWithParent:self.defaultContext];
            });
            
            result = savingContext;
        }
            break;
            
        default:
            break;
    }
    
    return result;
}

- (NSManagedObjectContext *)contextWithParent:(NSManagedObjectContext *)parentContext
{
    NSManagedObjectContext *result = [self createPrivateQueueContext];
    
    [result setParentContext:parentContext];
    
    return result;
}

- (NSManagedObjectContext *)createPrivateQueueContext
{
    NSManagedObjectContext *result = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    
    return result;
}


- (NSManagedObjectContext *)defaultContext
{
    if (_defaultContext == nil)
    {
        NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
        if (coordinator != nil)
        {
            _defaultContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
            [_defaultContext setPersistentStoreCoordinator: coordinator];
            _defaultContext.mergePolicy = NSOverwriteMergePolicy;
            _defaultContext.undoManager = nil;
        }
    }
    
    return _defaultContext;
}


#pragma mark - Save

- (void)saveWithBlock:(void(^)(NSManagedObjectContext *localContext))block completion:(void(^)(void))completion
{
    __block NSManagedObjectContext *savingContext = self.savingContext;
    
    [savingContext performBlock:^{
        block(savingContext);
        NSError *error = nil;
        [self saveContext:savingContext error:&error];
        if (completion != nil)
        {
            completion();
        }
    }];
}

- (void)saveWithBlockAndWait:(SMStorageContextBlock)block completion:(void(^)(void))completion
{
    __block NSManagedObjectContext *savingContext = self.savingContext;
    
    void(^blockSave)(void) = ^(void)
    {
        [savingContext performBlockAndWait:^{
            block(savingContext);
            NSError *error = nil;
            [self saveContext:savingContext error:&error];
            if (completion != nil)
            {
                completion();
            }
        }];
    };

    switch (self.saveType)
    {
        case SMStorageSaveTypeMultiContexts:
            blockSave();
            break;
        case SMStorageSaveTypeSingleContext:
            @synchronized (self)
            {
                blockSave();
            }
            break;
        default:
            break;
    }
}

- (void)saveWithBlockAndWait:(SMStorageContextBlock)block
{
    [self saveWithBlockAndWait:block completion:NULL];
}

- (void)saveContext:(NSManagedObjectContext *)aContext error:(NSError * __autoreleasing *)aError
{
    SMWeakSelf
    NSError *error = nil;
    if ([aContext hasChanges])
    {
        [aContext save:&error];
        if (error)
        {
            NSLog(@"SMStorage_: Unresolved error %@", error);
#if DEBUG
            abort();
#else
            [self.defaultContext rollback];
#endif
        } else if (aContext.parentContext)
        {
            [aContext.parentContext performBlockAndWait:^{
                [weakSelf saveContext:aContext.parentContext error:aError];
            }];
        }
    }
}


#pragma mark - Perform blocks

- (void)defaultContextBlockAndWait:(SMStorageContextBlock)aBlock
{
    [self.defaultContext performBlockAndWait:^{
        aBlock(self.defaultContext);
    }];
}

- (void)defaultContextBlock:(SMStorageContextBlock)aBlock
{
    [self.defaultContext performBlock:^{
        aBlock(self.defaultContext);
    }];
}


#pragma mark - Remove entities

- (void)removeAllEntitiesWithName:(NSString *)anEntityName
{
    [self saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        NSFetchRequest *entitiesRequest = [NSFetchRequest new];
        entitiesRequest.entity = [NSEntityDescription entityForName:anEntityName inManagedObjectContext:localContext];
        [entitiesRequest setIncludesPropertyValues:NO];
        NSError *error = nil;
        NSArray *entities = [localContext executeFetchRequest:entitiesRequest error:&error];
        if(error)
        {
            SMLog(@"STStorage: remove entities with error: %@", error);
        }
        for (NSManagedObject *object in entities)
        {
            [localContext deleteObject:object];
        }
    }];
}


#pragma mark - Clear

- (void)clear
{
    SMWeakSelf
    [self saveWithBlock:^(NSManagedObjectContext *localContext) {
        [weakSelf clearInContext:localContext];
    } completion:NULL];
}

- (void)clearAndWait
{
    SMWeakSelf
    [self saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        [weakSelf clearInContext:localContext];
    }];
}

- (void)clearInContext:(NSManagedObjectContext *)context
{
    NSArray *allEntities = self.managedObjectModel.entities;
    for (NSEntityDescription *entityDescription in allEntities)
    {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        [fetchRequest setEntity:entityDescription];
        
        fetchRequest.includesPropertyValues = NO;
        fetchRequest.includesSubentities = NO;
        
        NSError *error;
        NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
        
        if (error)
        {
            NSLog(@"Error requesting items from Core Data: %@", [error localizedDescription]);
        }
        
        for (NSManagedObject *managedObject in items)
        {
            [context deleteObject:managedObject];
        }
    }
}


#pragma mark - Remove

- (void)removeObject:(NSManagedObject <SMDBStorableObject>*)aObject
{
    [self saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        NSManagedObject *obj = [aObject inContext:localContext];
        [localContext deleteObject:obj];
    }];
}

- (void)removeObjects:(NSArray <NSManagedObject <SMDBStorableObject>*>*)aObjects
{
    [self saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        for (NSManagedObject <SMDBStorableObject> *object in aObjects)
        {
            NSManagedObject *obj = [object inContext:localContext];
            [localContext deleteObject:obj];
        }
    }];
}


#pragma mark - Clone

- (NSManagedObject *)cloneObject:(NSManagedObject *)anObject clonePolicy:(SMClonePolicy)aClonePoliy
{
    __block NSManagedObject *result;
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:anObject.entity.name
                                              inManagedObjectContext:self.defaultContext];
    
    switch (aClonePoliy)
    {
        case SMClonePolicyAsOriginal:
        {
            result = [[[anObject class] alloc] initWithEntity:entity insertIntoManagedObjectContext:anObject.managedObjectContext];
        }
            break;
        case SMClonePolicyAsTemp:
        {
            result = [[[anObject class] alloc] initWithEntity:entity insertIntoManagedObjectContext:nil];
        }
            break;
            
        case SMClonePolicyInsertIntoDefaultContext:
        {
            [self defaultContextBlockAndWait:^(NSManagedObjectContext *aContext) {
                result = [[[anObject class] alloc] initWithEntity:entity insertIntoManagedObjectContext:aContext];
            }];
        }
            break;
        default:
            break;
    }
    
    NSDictionary *attributes = [entity attributesByName];
    for(NSString *attributeName in attributes)
    {
        [result setValue:[anObject valueForKey:attributeName] forKey:attributeName];
    }
    
    return result;
}

@end

