//
//  SMDBStorableObject.h
//  Semenag01Kit
//
//  Created by Developer on 1/18/18.
//  Copyright © 2018 Developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@protocol SMDBStorableObject <NSObject>

/**
 * Change context
 **/
- (instancetype)inContext:(NSManagedObjectContext *)context;

@optional

/**
 * Setup unsafe with dictionary
 **/
- (void)setupWithDictionary:(NSDictionary *)aData context:(NSManagedObjectContext *)aContext;

/**
 * Create without insert into context
 **/
+ (instancetype)makeObjectTemp;

/**
 * Create in context without immediate save into storage
 **/
+ (instancetype)makeObjectContext:(NSManagedObjectContext *)aContext;

@end
