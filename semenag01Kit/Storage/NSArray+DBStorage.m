//
//  NSArray+DBStorage.m
//  Semenag01Kit
//
//  Created by OLEKSANDR SEMENIUK on 3/3/18.
//  Copyright © 2018 Developer. All rights reserved.
//

#import "NSArray+DBStorage.h"

@implementation NSArray (DBStorage)

- (NSMutableArray <NSManagedObject <SMDBStorableObject>*>*)moveToContext:(NSManagedObjectContext *)context
{
    NSMutableArray *result = [NSMutableArray new];
    
    for (NSManagedObject <SMDBStorableObject>* obj in self)
    {
        NSManagedObject *objInContext = [obj inContext:context];
        if (objInContext)
        {
            [result addObject:objInContext];
        }
    }
    
    return result;
}

@end

