//
//  SMCell.h
//  Semenag01Kit
//
//  Created by semenag01 on 30.03.12.
//  Copyright (c) 2012 semenag01. all rights reserved.
//

#import <UIKit/UIKit.h>
#import "SMCellProtocol.h"


@interface SMCell : UITableViewCell <SMCellProtocol>

- (NSArray <UIResponder *>*)inputTraits;

@end
