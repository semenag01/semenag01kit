//
//  SMCell.m
//  Semenag01Kit
//
//  Created by semenag01 on 30.03.12.
//  Copyright (c) 2012 semenag01. all rights reserved.
//

#import "SMCell.h"
#import "SMCellData.h"

@implementation SMCell

@synthesize cellData;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self)
    {
        self.backgroundColor = [UIColor clearColor];
    }
    
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];

    self.backgroundColor = [UIColor clearColor];
}

#pragma mark - Setup

- (void)setupCellData:(SMCellData *)aCellData
{
    if (cellData != aCellData)
    {
        cellData = aCellData;
    }
    
    if (aCellData.cellWidth > 0)
    {
        CGRect frame = self.frame;
        frame.size.width = aCellData.cellWidth;
        self.frame = frame;
        if (!self.superview)
        {
            self.contentView.frame = self.bounds;
        }
    }
    
    self.selectionStyle = aCellData.cellSelectionStyle;
    self.accessoryType = aCellData.cellAccessoryType;
    self.tag = cellData.tag;
    
    __weak __typeof(&*self)weakSelf = self;
    aCellData.baPrepareForReuse = [SMBlockAction blockActionsWithBlock:^(SMListCellData *sender) {
        
        [weakSelf prepareForReuse];
    }];
}

- (NSArray <UIResponder *>*)inputTraits
{
    return nil;
}

- (NSString *)reuseIdentifier
{
    return self.cellData.cellIdentifier;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.cellData.baPrepareForReuse = nil;
}

@end
