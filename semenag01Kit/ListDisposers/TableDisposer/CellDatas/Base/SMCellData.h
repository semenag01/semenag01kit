//
//  SMCellData.h
//  Semenag01Kit
//
//  Created by semenag01 on 3/29/12.
//  Copyright (c) 2012 semenag01. all rights reserved.
//


#import "SMListCellData.h"
#import "SMCellProtocol.h"

@class SMCell;

typedef enum _SMCellSeparatorStyle
{
    SMCellSeparatorStyleNone = 0,
    SMCellSeparatorStyleTop = 1 << 0,
    SMCellSeparatorStyleBottom = 1 << 1,
} SMCellSeparatorStyle;

@interface SMCellData : SMListCellData
{
    @private
    NSMutableArray *cellSelectedHandlers;
    NSMutableArray *cellDeselectedHandler;
}

@property (nonatomic, retain) NSString *cellNibName;

@property (nonatomic, assign) Class cellClass;


@property (nonatomic, assign) UITableViewCellStyle cellStyle;
@property (nonatomic, assign) UITableViewCellSelectionStyle cellSelectionStyle;
@property (nonatomic, assign) UITableViewCellAccessoryType cellAccessoryType;

@property (nonatomic, assign) BOOL isCellHeightAutomaticDimension;

@property (nonatomic, assign) CGFloat cellHeight;
@property (nonatomic, assign) CGFloat cellWidth;

@property (nonatomic, assign) SMCellSeparatorStyle cellSeparatorStyle;  // by default is SMCellSeparatorStyleNone
@property (nonatomic, retain) UIColor *cellTopSeparatorColor;           // by default is nil
@property (nonatomic, retain) UIColor *cellBottomSeparatorColor;        // by default is nil

- (CGFloat)cellHeightForWidth:(CGFloat) aWidth;

- (void)addCellSelectedTarget:(id)aTarget action:(SEL)anAction;
- (void)addCellDeselectedTarget:(id)aTarget action:(SEL)anAction;

- (void)performSelectedHandlers;
- (void)performDeselectedHandlers;

- (UITableViewCell <SMCellProtocol> *)createCell;

@end
