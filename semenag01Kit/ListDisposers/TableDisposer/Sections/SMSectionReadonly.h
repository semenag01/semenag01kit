//
//  SMSectionReadonly.h
//  Semenag01Kit
//
//  Created by semenag01 on 29.03.12.
//  Copyright (c) 2012 semenag01. all rights reserved.
//

#import <Foundation/Foundation.h>
#import "SMListSection.h"
#import "SMCellProtocol.h"

@class SMCell, SMTableDisposer;

@interface SMSectionReadonly : SMListSection

@property (nonatomic, strong) NSString *headerTitle;
@property (nonatomic, strong) NSString *footerTitle;
@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, strong) UIView *footerView;

@property (nonatomic, readonly) SMTableDisposer *tableDisposer;


#pragma mark - Init/Dealloc

- (UITableViewCell <SMCellProtocol> *)cellForIndex:(NSUInteger)anIndex;

- (void)reloadWithAnimation:(UITableViewRowAnimation)anAnimation;
- (void)reloadRowsAtIndexes:(NSArray*)anIndexes withAnimation:(UITableViewRowAnimation)aRowAnimation;
- (void)deleteRowsAtIndexes:(NSArray*)anIndexes withAnimation:(UITableViewRowAnimation)aRowAnimation;

- (void)showCellByIndex:(NSUInteger)anIndex needUpdateTable:(BOOL)aNeedUpdateTable;
- (void)showCellByIndex:(NSUInteger)anIndex needUpdateTable:(BOOL)aNeedUpdateTable withAnimation:(UITableViewRowAnimation)aRowAnimation;
- (void)hideCellByIndex:(NSUInteger)anIndex needUpdateTable:(BOOL)aNeedUpdateTable;
- (void)hideCellByIndex:(NSUInteger)anIndex needUpdateTable:(BOOL)aNeedUpdateTable withAnimation:(UITableViewRowAnimation)aRowAnimation;

@end
