//
//  SMSectionReadonly.m
//  Semenag01Kit
//
//  Created by semenag01 on 29.03.12.
//  Copyright (c) 2012 semenag01. all rights reserved.
//

#import "SMSectionWritable.h"
#import "SMCellDataMaped.h"
#import "SMCellData.h"
#import "SMCellProtocol.h"
#import "SMTableDisposer.h"
#import "SMKeyboardAvoidingTableView.h"
#import "SMKeyboardAvoidingProtocol.h"
#import "SMCell.h"

@interface SMSectionWritable ()

- (UITableViewCell <SMCellProtocol> *)createCellAtIndex:(NSUInteger)anIndex;

@end


@implementation SMSectionWritable

#pragma mark - Init/Dealloc

- (instancetype)init
{
    if( (self = [super init]) )
    {
        cells = [NSMutableArray new];
    }
    return self;
}

#pragma mark - Cells

- (void)createCells
{
    // remove old cells
    [cells removeAllObjects];
    
    [self updateCellDataVisibility];

    UITableViewCell <SMCellProtocol> *cell;
    for (NSUInteger index = 0; index < visibleCellDataSource.count; ++index)
    {
        cell = [self createCellAtIndex:index];
        [cells addObject:cell];
    }
}

- (UITableViewCell<SMCellProtocol> *)cellForIndex:(NSUInteger)anIndex
{
    UITableViewCell<SMCellProtocol> *cell = [cells objectAtIndex:anIndex];
    // ...
    return cell;
}

- (void)reloadWithAnimation:(UITableViewRowAnimation)anAnimation
{
    if ([self.tableDisposer.tableView conformsToProtocol:@protocol(SMKeyboardAvoidingProtocol)])
    {
        for (UITableViewCell <SMCellProtocol> *cell in cells)
        {
            if ([cell isKindOfClass:[SMCell class]])
            {
                SMCell *c = (SMCell *)cell;
                [(id<SMKeyboardAvoidingProtocol>)self.tableDisposer.tableView removeObjectsForKeyboard:[c inputTraits]];
            }
        }
    }

    [self mapFromObject];
    
    [super reloadWithAnimation:anAnimation];
}

- (void)reloadRowsAtIndexes:(NSArray *)anIndexes withAnimation:(UITableViewRowAnimation)aRowAnimation
{
    NSMutableArray *indexPaths = [NSMutableArray array];
    NSIndexPath *indexPath;
    NSInteger sectionIndex = [self.tableDisposer indexBySection:self];
    
    SMListCellData *cellData;
    UITableViewCell <SMCellProtocol> *cell;
    for(NSNumber *index in anIndexes)
    {
        cellData = [self visibleCellDataAtIndex:[index integerValue]];
        if([cellData isKindOfClass:[SMCellDataMaped class]])
        {
            [(SMCellDataMaped*)cellData mapFromObject];
        }
        
        cell = [self cellForIndex:[index integerValue]];
        [cell setupCellData:cellData];

        indexPath = [NSIndexPath indexPathForRow:[index integerValue] inSection:sectionIndex];
        [indexPaths addObject:indexPath];
    }
    
    [self.tableDisposer.tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:aRowAnimation];
}


#pragma mark - Show/Hide cels

- (void)hideCellByIndex:(NSUInteger)anIndex needUpdateTable:(BOOL)aNeedUpdateTable withAnimation:(UITableViewRowAnimation)aRowAnimation
{
    SMListCellData *cellData = [self cellDataAtIndex:anIndex];
    if(!cellData.isVisible)
        return;
    
    NSUInteger index = [self indexByVisibleCellData:cellData];
    
    UITableViewCell <SMCellProtocol> *cell = [self cellForIndex:index];
    if ([self.tableDisposer.tableView conformsToProtocol:@protocol(SMKeyboardAvoidingProtocol)])
    {
        if ([cell isKindOfClass:[SMCell class]])
        {
            SMCell *c = (SMCell *)cell;
            [((id<SMKeyboardAvoidingProtocol>)self.tableDisposer.tableView) removeObjectsForKeyboard:[c inputTraits]];
        }
    }
    
    [visibleCellDataSource removeObjectAtIndex:index];
    [cells removeObjectAtIndex:index];
    cellData.isVisible = NO;
    
    if(aNeedUpdateTable)
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:[self.tableDisposer indexBySection:self]];
        [self.tableDisposer.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:aRowAnimation];
    }
}

- (void)showCellByIndex:(NSUInteger)anIndex needUpdateTable:(BOOL)aNeedUpdateTable withAnimation:(UITableViewRowAnimation)aRowAnimation
{
    SMListCellData *cellData = [self cellDataAtIndex:anIndex];
    if(cellData.isVisible)
        return;

    cellData.isVisible = YES;
    [self updateCellDataVisibility];
    
    NSUInteger index = [self indexByVisibleCellData:cellData];
    UITableViewCell <SMCellProtocol> *cell = [self createCellAtIndex:index];
    [cells insertObject:cell atIndex:index];

    if(aNeedUpdateTable)
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:[self.tableDisposer indexBySection:self]];
        [self.tableDisposer.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:aRowAnimation];
    }
}

#pragma mark - Maping

- (void)mapFromObject
{
    for (SMCellData *cellData in cellDataSource)
    {
        if ([cellData isKindOfClass:[SMCellDataMaped class]])
            [(SMCellDataMaped*)cellData mapFromObject];
    }
    [self createCells];
}

- (void)mapToObject
{
    for (SMCellData *cellData in cellDataSource)
    {
        if ([cellData isKindOfClass:[SMCellDataMaped class]])
            [(SMCellDataMaped*)cellData mapToObject];
    }
}

- (void)deleteRowsAtIndexes:(NSArray*)anIndexes withAnimation:(UITableViewRowAnimation)aRowAnimation
{
    [super deleteRowsAtIndexes:anIndexes withAnimation:aRowAnimation];
    
    NSMutableIndexSet *set = [NSMutableIndexSet new];
    for(NSNumber *index in anIndexes)
    {
        [set addIndex:[index unsignedIntegerValue]];
    }
    [cells removeObjectsAtIndexes:set];
}

#pragma mark - Private

- (UITableViewCell <SMCellProtocol> *)createCellAtIndex:(NSUInteger)anIndex
{
    SMCellData *cellData = (SMCellData *)[self visibleCellDataAtIndex:anIndex];
    
    UITableViewCell <SMCellProtocol> *cell;
    if (cellData.cellNibName || cellData.cellClass)
    {
        cell = [cellData createCell];
    } else
    {
        cell = [self.tableDisposer.tableView dequeueReusableCellWithIdentifier:cellData.cellIdentifier];//Stroryboard
    }
    
    [cell setupCellData:cellData];
    
    if ([self.tableDisposer.tableView conformsToProtocol:@protocol(SMKeyboardAvoidingProtocol)])
    {
        if ([cell isKindOfClass:[SMCell class]])
        {
            SMCell *c = (SMCell *)cell;
            [((id<SMKeyboardAvoidingProtocol>)self.tableDisposer.tableView) addObjectsForKeyboard:[c inputTraits]];
            [((SMKeyboardAvoidingTableView*)self.tableDisposer.tableView) sordetInputTraits:[c inputTraits] byIndexPath:[NSIndexPath indexPathForRow:anIndex inSection:[self.tableDisposer indexBySection:self]]];
        }
    }
    
    [self.tableDisposer didCreateCell:cell];
    
    return cell;
}

@end
