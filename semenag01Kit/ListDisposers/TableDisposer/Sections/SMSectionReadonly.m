//
//  SMSectionReadonly.m
//  Semenag01Kit
//
//  Created by semenag01 on 29.03.12.
//  Copyright (c) 2012 semenag01. all rights reserved.
//

#import "SMSectionReadonly.h"
#import "SMTableDisposer.h"
#import "SMCellProtocol.h"

@implementation SMSectionReadonly

@synthesize headerTitle;
@synthesize footerTitle;
@synthesize headerView;
@synthesize footerView;


#pragma mark - Cells

- (SMTableDisposer *)tableDisposer
{
    return (SMTableDisposer *)disposer;
}

- (UITableViewCell <SMCellProtocol> *)cellForIndex:(NSUInteger)anIndex
{
    UITableViewCell <SMCellProtocol> *cell = nil;
    
    BOOL isNewCell = NO;
    
    SMCellData *cellData = (SMCellData *)[self visibleCellDataAtIndex:anIndex];
    
    cell = [self.tableDisposer.tableView dequeueReusableCellWithIdentifier:cellData.cellIdentifier];
    
    if(!cell)
    {
        isNewCell = YES;
        cell = [cellData createCell];
    }
    
    if ([cell conformsToProtocol:@protocol(SMCellProtocol)])
    {
        [cell setupCellData:cellData];
    }
    
    if(isNewCell)
    {
        [self.tableDisposer didCreateCell:cell];
    }
    
    return cell;
}

- (void)reloadWithAnimation:(UITableViewRowAnimation)anAnimation
{
    [self updateCellDataVisibility];
    [self.tableDisposer.tableView reloadSections:[NSIndexSet indexSetWithIndex:[self.tableDisposer indexBySection:self]] withRowAnimation:anAnimation];
}

- (void)reloadRowsAtIndexes:(NSArray *)anIndexes withAnimation:(UITableViewRowAnimation)aRowAnimation
{
    NSMutableArray *indexPaths = [NSMutableArray array];
    NSIndexPath *indexPath;
    NSInteger sectionIndex = [self.tableDisposer indexBySection:self];
    for(NSNumber *index in anIndexes)
    {
        indexPath = [NSIndexPath indexPathForRow:[index integerValue] inSection:sectionIndex];
        [indexPaths addObject:indexPath];
    }
    
    [self.tableDisposer.tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:aRowAnimation];
}

- (void)deleteRowsAtIndexes:(NSArray *)anIndexes withAnimation:(UITableViewRowAnimation)aRowAnimation
{
    NSMutableArray *toDelete = [NSMutableArray array];
    SMListCellData *cellData;
    for(NSNumber *index in anIndexes)
    {
        cellData = [self cellDataAtIndex:[index intValue]];
        [toDelete addObject:cellData];
    }
    
    [cellDataSource removeObjectsInArray:toDelete];
    [self updateCellDataVisibility];
    
    NSMutableArray *indexPaths = [NSMutableArray array];
    NSIndexPath *indexPath;
    NSInteger sectionIndex = [self.tableDisposer indexBySection:self];
    for(NSNumber *index in anIndexes)
    {
        indexPath = [NSIndexPath indexPathForRow:[index integerValue] inSection:sectionIndex];
        [indexPaths addObject:indexPath];
    }
    [self.tableDisposer.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:aRowAnimation];
}


#pragma mark - Show/Hide cels

- (void)hideCellByIndex:(NSUInteger)anIndex needUpdateTable:(BOOL)aNeedUpdateTable
{
    [self hideCellByIndex:anIndex needUpdateTable:aNeedUpdateTable withAnimation:UITableViewRowAnimationMiddle];
}

- (void)showCellByIndex:(NSUInteger)anIndex needUpdateTable:(BOOL)aNeedUpdateTable
{
    [self showCellByIndex:anIndex needUpdateTable:aNeedUpdateTable withAnimation:UITableViewRowAnimationMiddle];
}

- (void)hideCellByIndex:(NSUInteger)anIndex needUpdateTable:(BOOL)aNeedUpdateTable withAnimation:(UITableViewRowAnimation)aRowAnimation
{
    SMCellData *cellData = (SMCellData *)[self cellDataAtIndex:anIndex];
    if(!cellData.isVisible)
        return;
    
    NSUInteger index = [self indexByVisibleCellData:cellData];
    
    [visibleCellDataSource removeObjectAtIndex:index];
    cellData.isVisible = NO;
    
    if(aNeedUpdateTable)
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:[self.tableDisposer indexBySection:self]];
        [self.tableDisposer.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:aRowAnimation];
    }
}

- (void)showCellByIndex:(NSUInteger)anIndex needUpdateTable:(BOOL)aNeedUpdateTable withAnimation:(UITableViewRowAnimation)aRowAnimation
{
    SMCellData *cellData = (SMCellData *)[self cellDataAtIndex:anIndex];
    if(cellData.isVisible)
        return;
    
    cellData.isVisible = YES;
    [self updateCellDataVisibility];
    
    NSUInteger index = [self indexByVisibleCellData:cellData];
    
    if(aNeedUpdateTable)
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:[self.tableDisposer indexBySection:self]];
        [self.tableDisposer.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:aRowAnimation];
    }
}

@end
