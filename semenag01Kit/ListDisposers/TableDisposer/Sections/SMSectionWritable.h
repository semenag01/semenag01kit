//
//  SMSectionReadonly.h
//  Semenag01Kit
//
//  Created by semenag01 on 29.03.12.
//  Copyright (c) 2012 semenag01. all rights reserved.
//

#import "SMSectionReadonly.h"

@interface SMSectionWritable : SMSectionReadonly
{
    NSMutableArray *cells;
}

- (void)createCells;

- (void)mapFromObject;
- (void)mapToObject;

@end
