//
//  SMTableDisposerModeled.h
//
//
//  Created by semenag01 on 30.03.13.
//  Copyright (c) 2013 semenag01. all rights reserved.
//

#import "SMTableDisposer.h"
#import "SMListDisposerSetupModelProtocol.h"
#import "SMListDisposerModeledDelegate.h"


@interface SMTableDisposerModeled : SMTableDisposer <SMListDisposerSetupModelProtocol>
{
    NSMutableDictionary <NSString *, Class> *registeredClasses;
}

@end
