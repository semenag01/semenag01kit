//
//  SMTableDisposerModeled.m
//
//
//  Created by semenag01 on 30.03.13.
//  Copyright (c) 2013 semenag01. all rights reserved.
//

#import "SMTableDisposerModeled.h"

@implementation SMTableDisposerModeled

@synthesize modeledDelegate, modeledMulticastDelegate;

- (instancetype)init
{
    self = [super init];
    
    if(self)
    {
        registeredClasses = [NSMutableDictionary new];
        modeledMulticastDelegate = (SMMulticastDelegate<SMListDisposerModeledCreateCellDataDelegate>*)[SMMulticastDelegate new];
    }
    
    return self;
}


#pragma mark - Modeled multicast delegates

- (void)addModeledDelegate:(id)aDelegate delegateQueue:(dispatch_queue_t)aDelegateQueue
{
    [modeledMulticastDelegate addDelegate:aDelegate delegateQueue:aDelegateQueue];
}

- (void)removeModeledDelegate:(id)aDelegate delegateQueue:(dispatch_queue_t)aDelegateQueue
{
    [modeledMulticastDelegate removeDelegate:aDelegate delegateQueue:aDelegateQueue];
}


#pragma mark - SMListDisposerSetupModelProtocol

- (SMCellData *)cellDataFromModel:(id)aModel
{
    NSString *modelClassName = NSStringFromClass([aModel class]);
    
    Class cellDataClass = [registeredClasses objectForKey:modelClassName];
    
    if(!cellDataClass && modeledDelegate && [modeledDelegate respondsToSelector:@selector(listDisposer:cellDataClassForUnregisteredModel:)])
    {
        cellDataClass = [modeledDelegate listDisposer:self cellDataClassForUnregisteredModel:aModel];
    }
    
    NSAssert(cellDataClass, (NSString *)([NSString stringWithFormat:@"Model doesn't have registered cellData class %@", NSStringFromClass([aModel class])]));
    
    SMCellData *cellData = [[cellDataClass alloc] initWithModel:aModel];
    
    return cellData;
}

- (void)registerCellData:(Class)aCellDataClass forModel:(Class)aModelClass
{
    if (aModelClass != Nil)
    {
        [registeredClasses setObject:aCellDataClass forKey:NSStringFromClass(aModelClass)];
    }
}

- (void)unregisterCellDataForModel:(Class)aModelClass
{
    [registeredClasses removeObjectForKey:NSStringFromClass(aModelClass)];
}

- (void)setupModels:(NSArray*)aModels forSectionAtIndex:(NSUInteger)aSectionIndex
{
    SMSectionReadonly *section = (SMSectionReadonly *)[self sectionByIndex:aSectionIndex];
    [self setupModels:aModels forSection:section];
}

- (void)setupModels:(NSArray *)aModels forSection:(SMSectionReadonly *)aSection
{
    NSAssert(aSection, @"aSection is nil!!!");
    
    for(id model in aModels)
    {
        SMListCellData *cellData = [self cellDataFromModel:model];
        if(cellData)
        {
            [aSection addCellData:cellData];
            
            [self didCreateCellData:cellData];
        }
    }
}


#pragma mark -

- (void)didCreateCellData:(SMListCellData *)aCellData
{
    if([self.modeledDelegate respondsToSelector:@selector(listDisposer:didCreateCellData:)])
    {
        [self.modeledDelegate listDisposer:self didCreateCellData:aCellData];
    }
    
    [modeledMulticastDelegate listDisposer:self didCreateCellData:aCellData];
}


#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone
{
    SMTableDisposerModeled *tableDisposer = (SMTableDisposerModeled*)[super copyWithZone:zone];
    tableDisposer.modeledDelegate = self.modeledDelegate;
    [tableDisposer->registeredClasses addEntriesFromDictionary:registeredClasses];
    
    SMMulticastDelegateEnumerator *enumerator = [self.modeledMulticastDelegate delegateEnumerator];
    id nextDelegate; dispatch_queue_t nextDispatch_queue;
    while ([enumerator getNextDelegate:&nextDelegate delegateQueue:&nextDispatch_queue])
    {
        [tableDisposer.modeledMulticastDelegate addDelegate:nextDelegate delegateQueue:nextDispatch_queue];
    }
    
    return tableDisposer;
}

@end
