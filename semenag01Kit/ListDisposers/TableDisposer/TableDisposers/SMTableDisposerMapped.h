//
//  SMTableDisposerMapped.h
//  Semenag01Kit
//
//  Created by semenag01 on 30.03.12.
//  Copyright (c) 2012 semenag01. all rights reserved.
//

#import "SMTableDisposer.h"

@interface SMTableDisposerMapped : SMTableDisposer

- (void)mapToObject;

@end
