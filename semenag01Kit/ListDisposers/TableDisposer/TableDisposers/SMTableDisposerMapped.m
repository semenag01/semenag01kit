//
//  SMTableDisposerMaped.m
//  Semenag01Kit
//
//  Created by semenag01 on 30.03.12.
//  Copyright (c) 2012 semenag01. all rights reserved.
//

#import "SMTableDisposerMapped.h"

#import "SMCellDataMaped.h"
#import "SMKeyboardAvoidingTableView.h"

@interface SMTableDisposerMapped ()

- (void)mapFromObject;
@end

@implementation SMTableDisposerMapped

- (void)mapFromObject
{
    for(SMSectionReadonly *section in sections)
    {
        if([section isKindOfClass:[SMSectionWritable class]])
        {
            [(SMSectionWritable *)section mapFromObject];
        }
    }
}

- (void)mapToObject
{
    for(SMSectionReadonly *section in sections)
    {
        if([section isKindOfClass:[SMSectionWritable class]])
        {
            [(SMSectionWritable*)section mapToObject];
        }
    }
}

- (void)reloadData
{
    if([self.tableView isKindOfClass:[SMKeyboardAvoidingTableView class]])
    {
        [((SMKeyboardAvoidingTableView *)self.tableView) removeAllObjectsForKeyboard];
    }
    
    [self mapFromObject];
    
    for(SMSectionReadonly *section in sections)
    {
        [section updateCellDataVisibility];
    }
    
    [self.tableView reloadData];
}

@end
