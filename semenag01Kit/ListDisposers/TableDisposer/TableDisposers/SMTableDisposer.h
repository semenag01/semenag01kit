//
//  SMTableDisposer.h
//  Semenag01Kit
//
//  Created by semenag01 on 30.03.12.
//  Copyright (c) 2012 semenag01. all rights reserved.
//

#import <Foundation/Foundation.h>
#import "SMSectionWritable.h"
#import "SMCellData.h"
#import "SMMulticastDelegate.h"
#import "SMCellProtocol.h"
#import "SMListDisposer.h"

@protocol SMTableDisposerDelegate;
@protocol SMTableDisposerMulticastDelegate;

@interface SMTableDisposer : SMListDisposer <UITableViewDataSource, UITableViewDelegate, NSCopying>
{

}

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, weak) id<SMTableDisposerDelegate> delegate;
@property (nonatomic, readonly) SMMulticastDelegate<SMTableDisposerMulticastDelegate> *multicastDelegate;


#pragma mark - Multicast Delegates
- (void)addDelegate:(id)aDelegate delegateQueue:(dispatch_queue_t)aDelegateQueue;
- (void)removeDelegate:(id)aDelegate delegateQueue:(dispatch_queue_t)aDelegateQueue;


#pragma mark - Show/Hide cellData
- (void)showCellByIndexPath:(NSIndexPath*)anIndexPath needUpdateTable:(BOOL)aNeedUpdateTable;
- (void)hideCellByIndexPath:(NSIndexPath*)anIndexPath needUpdateTable:(BOOL)aNeedUpdateTable;

- (void)showCellByIndexPath:(NSIndexPath*)anIndexPath
            needUpdateTable:(BOOL)aNeedUpdateTable
           withRowAnimation:(UITableViewRowAnimation)aTableViewRowAnimation;

- (void)hideCellByIndexPath:(NSIndexPath*)anIndexPath
            needUpdateTable:(BOOL)aNeedUpdateTable
           withRowAnimation:(UITableViewRowAnimation)aTableViewRowAnimation;


#pragma mark - Deletions
- (void)deleteRowsAtIndexPaths:(NSArray*)anIndexPaths withRowAnimation:(UITableViewRowAnimation)aTableViewRowAnimation;

#pragma mark - Data reloading
- (void)reloadSectionsWithAnimation:(UITableViewRowAnimation)anAnimation;

#pragma mark -
- (void)scrollToBottom:(BOOL)anAnimated;

#pragma mark -
- (void)didCreateCell:(UITableViewCell <SMCellProtocol> *)aCell;
@end



@protocol SMTableDisposerDelegate <UITableViewDelegate>

@optional

- (BOOL)tableView:(UITableView *)aTableView canEditRowAtIndexPath:(NSIndexPath *)indexPath;
- (BOOL)tableView:(UITableView *)aTableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath;
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)aTableView;
- (NSInteger)tableView:(UITableView *)aTableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index;
- (void)tableView:(UITableView *)aTableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)tableView:(UITableView *)aTableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath;

- (void)tableDisposer:(SMTableDisposer*)aTableDisposer didCreateCell:(UITableViewCell <SMCellProtocol> *)aCell;

@end


@protocol SMTableDisposerMulticastDelegate <NSObject>

@optional
- (void)tableDisposer:(SMTableDisposer*)aTableDisposer didCreateCell:(UITableViewCell <SMCellProtocol> *)aCell;
- (void)tableView:(UITableView *)aTableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath;

@end
