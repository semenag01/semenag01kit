//
//  SMListDisposer.m
//  semenag01Kit
//
//  Created by OLEKSANDR SEMENIUK on 8/3/18.
//  Copyright © 2018 semenag01. All rights reserved.
//

#import "SMListDisposer.h"
#import "SMListCellData.h"
#import "SMListSection.h"


@implementation SMListDisposer
@synthesize listView;

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        sections = [NSMutableArray new];
    }
    
    return self;
}

- (void)reloadData
{
    assert(NO);
}

#pragma mark - Sections

- (void)addSection:(SMListSection *)aSection
{
    NSParameterAssert(aSection);
    [sections addObject:aSection];
    aSection.disposer = self;
}

- (void)insertSection:(SMListSection *)aSection atIndex:(NSUInteger)anIndex needUpdateTable:(BOOL)aNeedUpdateTable
{
    NSParameterAssert(aSection);
    [sections insertObject:aSection atIndex:anIndex];
    aSection.disposer = self;
}

- (void)removeSectionAtIndex:(NSUInteger)anIndex needUpdateTable:(BOOL)aNeedUpdateTable
{
    [sections removeObjectAtIndex:anIndex];
}

- (void)removeSection:(SMListSection *)aSection needUpdateTable:(BOOL)aNeedUpdateTable
{
    NSUInteger index = [self indexBySection:aSection];
    [self removeSectionAtIndex:index needUpdateTable:aNeedUpdateTable];
}

- (void)removeAllSections
{
    [sections removeAllObjects];
}

- (void)removeAllSectionsAndCleanCells
{
    for (SMListSection *section in sections)
    {
        for (SMListCellData *cd in section.cellDataSource)
        {
            [cd.baPrepareForReuse performBlockFromSender:cd];
        }
    }
    
    [self removeAllSections];
}

- (SMListSection *)sectionByIndex:(NSUInteger)anIndex
{
    SMListSection *result = nil;
    
    if (anIndex < sections.count)
    {
        result = [sections objectAtIndex:anIndex];
    }
    
    return result;
}

- (NSUInteger)indexBySection:(SMListSection *)aSection
{
    return [sections indexOfObject:aSection];
}

- (NSUInteger)sectionsCount
{
    return [sections count];
}

- (NSArray <SMListSection *>*)sections
{
    return [NSArray arrayWithArray:sections];
}


#pragma mark - Search CellData & IndexPath

- (NSIndexPath *)indexPathByCellData:(SMListCellData *)aCellData
{
    NSIndexPath *result = nil;
    
    NSUInteger rowIndex = NSNotFound;
    NSUInteger secIndex = 0;
    for(SMListSection *section in sections)
    {
        rowIndex = [section indexByCellData:aCellData];
        if(rowIndex != NSNotFound)
        {
            result = [NSIndexPath indexPathForRow:rowIndex inSection:secIndex];
        }
        secIndex++;
    }
    
    return result;
}

- (NSIndexPath *)indexPathByVisibleCellData:(SMListCellData *)aCellData
{
    NSIndexPath *result = nil;
    
    NSUInteger rowIndex = NSNotFound;
    NSUInteger secIndex = 0;
    for(SMListSection *section in sections)
    {
        rowIndex = [section indexByVisibleCellData:aCellData];
        if(rowIndex != NSNotFound)
        {
            result = [NSIndexPath indexPathForRow:rowIndex inSection:secIndex];
        }
        secIndex++;
    }
    
    return result;
}

- (SMListCellData *)cellDataByIndexPath:(NSIndexPath*)anIndexPath
{
    return [[self sectionByIndex:anIndexPath.section] cellDataAtIndex:anIndexPath.row];
}

- (SMListCellData *)visibleCellDataByIndexPath:(NSIndexPath*)anIndexPath
{
    return [[self sectionByIndex:anIndexPath.section] visibleCellDataAtIndex:anIndexPath.row];
}

- (SMListCellData *)cellDataByTag:(NSUInteger)aTag
{
    SMListCellData *result = nil;
    
    for(SMListSection *section in sections)
    {
        result = [section cellDataByTag:aTag];
        if(result)
            break;
    }
    
    return result;
}

- (void)resetupCellWithCellData:(SMListCellData *)aCellData
{
    NSIndexPath *indexPath = [self indexPathByCellData:aCellData];
    
    [[self cellForIndexPath:indexPath] setupCellData:aCellData];
}

- (UIView <SMCellProtocol> *)cellForIndexPath:(NSIndexPath *)aIndexPath
{
    return nil;
}

- (void)replaceCellData:(SMListCellData *)aCellData indexPath:(NSIndexPath *)aIndexPath
{
    [self.sections[aIndexPath.section] removeCellDataAtIndex:aIndexPath.row];
    [self.sections[aIndexPath.section] insertCellData:aCellData atIndex:aIndexPath.row];
    
    [self reloadRows:@[aIndexPath]];
}

- (void)reloadRows:(NSArray <NSIndexPath *> *)aIndexPathes
{
    
}

@end
