//
//  SMListDisposerSetupModelProtocol.h
//  semenag01Kit
//
//  Created by OLEKSANDR SEMENIUK on 10/19/18.
//  Copyright © 2018 semenag01. All rights reserved.
//

#ifndef SMListDisposerSetupModelProtocol_h
#define SMListDisposerSetupModelProtocol_h

@protocol SMListDisposerModeledDelegate, SMListDisposerModeledCreateCellDataDelegate;
@class SMListSection, SMListCellData, SMMulticastDelegate;
@protocol SMListDisposerSetupModelProtocol <NSObject>

@property (nonatomic, weak) id<SMListDisposerModeledDelegate> modeledDelegate;
@property (nonatomic, readonly) SMMulticastDelegate<SMListDisposerModeledCreateCellDataDelegate> *modeledMulticastDelegate;


#pragma mark -

- (void)didCreateCellData:(SMListCellData *)aCellData;
- (SMListCellData *)cellDataFromModel:(id)aModel;


- (void)registerCellData:(Class)aCellDataClass forModel:(Class)aModelClass;
- (void)unregisterCellDataForModel:(Class)aModelClass;

- (void)setupModels:(NSArray *)aModels forSectionAtIndex:(NSUInteger)aSectionIndex;
- (void)setupModels:(NSArray *)aModels forSection:(SMListSection *)aSection;


#pragma mark - Modeled multicast delegates

- (void)addModeledDelegate:(id)aDelegate delegateQueue:(dispatch_queue_t)aDelegateQueue;
- (void)removeModeledDelegate:(id)aDelegate delegateQueue:(dispatch_queue_t)aDelegateQueue;

@end

#endif /* SMListDisposerSetupModelProtocol_h */
