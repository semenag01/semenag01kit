//
//  SMCellProtocol.h
//  Semenag01Kit
//
//  Created by semenag01 on 3/29/12.
//  Copyright (c) 2012 semenag01. all rights reserved.
//

#ifndef Semenag01Kit_SMCellProtocol_h
#define Semenag01Kit_SMCellProtocol_h


@class SMListCellData;

@protocol SMCellProtocol <NSObject>

@property (nonatomic, readonly) SMListCellData *cellData;

- (void)setupCellData:(SMListCellData *)aCellData;

@end

#endif
