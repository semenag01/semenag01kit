//
//  SMListCellData.m
//  semenag01Kit
//
//  Created by OLEKSANDR SEMENIUK on 8/3/18.
//  Copyright © 2018 semenag01. All rights reserved.
//

#import "SMListCellData.h"

@implementation SMListCellData
@synthesize model, isAutoDeselect;

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        [self setup];
    }
    
    return self;
}

- (instancetype)initWithModel:(id)aModel
{
    self = [super init];
    
    if (self)
    {
        [self setup];
        model = aModel;
    }
    
    return self;
}

- (void)setup
{
    self.isAutoDeselect = YES;
    self.isVisible = YES;
    self.userData = [NSMutableDictionary new];
    self.tag = 0;
}

@end
