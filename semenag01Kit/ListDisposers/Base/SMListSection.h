//
//  SMListSection.h
//  semenag01Kit
//
//  Created by OLEKSANDR SEMENIUK on 8/3/18.
//  Copyright © 2018 semenag01. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SMListCellData, SMListDisposer;

@interface SMListSection : NSObject
{
    NSMutableArray <SMListCellData *> *cellDataSource;
    NSMutableArray <SMListCellData *> *visibleCellDataSource;
    __weak SMListDisposer *disposer;
}

@property(nonatomic, weak) SMListDisposer *disposer;
@property (nonatomic, assign) NSInteger tag;
@property(nonatomic, readonly) NSArray <SMListCellData *> *cellDataSource;

+ (instancetype)section;

- (void)addCellData:(SMListCellData *)aCellData;
- (void)addCellDataFromArray:(NSArray*)aCellDataArray;
- (void)insertCellData:(SMListCellData *)aCellData atIndex:(NSUInteger)anIndex;
- (void)removeCellDataAtIndex:(NSUInteger)anIndex;
- (void)removeCellData:(SMListCellData *)aCellData;
- (void)removeAllCellData;
- (NSUInteger)cellDataCount;
- (NSUInteger)visibleCellDataCount;

- (SMListCellData *)cellDataAtIndex:(NSUInteger)anIndex;
- (SMListCellData *)visibleCellDataAtIndex:(NSUInteger)anIndex;
- (NSUInteger)indexByCellData:(SMListCellData *)aCellData;
- (NSUInteger)indexByVisibleCellData:(SMListCellData *)aCellData;
- (SMListCellData *)cellDataByTag:(NSUInteger)aTag;

- (void)updateCellDataVisibility;

@end
