//
//  SMListDisposer.h
//  semenag01Kit
//
//  Created by OLEKSANDR SEMENIUK on 8/3/18.
//  Copyright © 2018 semenag01. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SMCellProtocol.h"


@class SMListSection, SMListCellData;

@interface SMListDisposer : NSObject
{
    UIScrollView *listView;
    NSMutableArray <SMListSection *> *sections;
}

@property (nonatomic, strong) UIScrollView *listView;

- (void)reloadData;


#pragma mark - Sections

- (void)addSection:(SMListSection *)aSection;
- (void)insertSection:(SMListSection *)aSection atIndex:(NSUInteger)anIndex needUpdateTable:(BOOL)aNeedUpdateTable;
- (void)removeSectionAtIndex:(NSUInteger)anIndex needUpdateTable:(BOOL)aNeedUpdateTable;
- (void)removeSection:(SMListSection *)aSection needUpdateTable:(BOOL)aNeedUpdateTable;
- (void)removeAllSections;
- (void)removeAllSectionsAndCleanCells;
- (SMListSection *)sectionByIndex:(NSUInteger)anIndex;
- (NSUInteger)indexBySection:(SMListSection *)aSection;
- (NSUInteger)sectionsCount;
- (NSArray <SMListSection *>*)sections;


#pragma mark - Search CellData & IndexPath

- (NSIndexPath *)indexPathByCellData:(SMListCellData *)aCellData;
- (NSIndexPath *)indexPathByVisibleCellData:(SMListCellData *)aCellData;
- (SMListCellData *)cellDataByIndexPath:(NSIndexPath*)anIndexPath;
- (SMListCellData *)visibleCellDataByIndexPath:(NSIndexPath*)anIndexPath;
- (SMListCellData *)cellDataByTag:(NSUInteger)aTag;
- (void)resetupCellWithCellData:(SMListCellData *)aCellData;

- (UIView <SMCellProtocol> *)cellForIndexPath:(NSIndexPath *)aIndexPath;
- (void)replaceCellData:(SMListCellData *)aCellData indexPath:(NSIndexPath *)aIndexPath;
- (void)reloadRows:(NSArray <NSIndexPath *> *)aIndexPathes;

@end
