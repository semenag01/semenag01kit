//
//  SMListCellData.h
//  semenag01Kit
//
//  Created by OLEKSANDR SEMENIUK on 8/3/18.
//  Copyright © 2018 semenag01. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SMTargetAction.h"

@interface SMListCellData : NSObject
{
    id model;
}

@property (nonatomic, assign) BOOL isAutoDeselect;
@property (nonatomic, strong) SMBlockAction *baSelect;
@property (nonatomic, readonly) id model;
@property (nonatomic, assign) BOOL isVisible;
@property (nonatomic, assign) NSInteger tag;
@property (nonatomic, strong) NSMutableDictionary *userData;

@property (nonatomic, assign) BOOL isEnableEdit;
@property (nonatomic, assign) BOOL isDisableInputTraits;

@property (nonatomic, readonly) NSString *cellIdentifier;

@property (nonatomic, strong) SMBlockAction <SMListCellData *> *baPrepareForReuse;

- (instancetype)initWithModel:(id)aModel;
- (void)setup;


@end
