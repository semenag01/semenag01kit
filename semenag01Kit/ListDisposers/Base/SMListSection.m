//
//  SMListSection.m
//  semenag01Kit
//
//  Created by OLEKSANDR SEMENIUK on 8/3/18.
//  Copyright © 2018 semenag01. All rights reserved.
//

#import "SMListSection.h"
#import "SMListCellData.h"
#import "SMListDisposer.h"

@implementation SMListSection
@synthesize disposer, cellDataSource;

+ (instancetype)section
{
    return [[self class] new];
}

- (instancetype)init
{
    self = [super init];
     
    if(self)
    {
        cellDataSource = [NSMutableArray new];
        visibleCellDataSource = [NSMutableArray new];
    }
    
    return self;
}


#pragma mark - CellDatas

- (void)addCellData:(SMListCellData *)aCellData
{
    [cellDataSource addObject:aCellData];
}

- (void)addCellDataFromArray:(NSArray*)aCellDataArray
{
    [cellDataSource addObjectsFromArray:aCellDataArray];
}

- (void)insertCellData:(SMListCellData *)aCellData atIndex:(NSUInteger)anIndex
{
    [cellDataSource insertObject:aCellData atIndex:anIndex];
}

- (void)removeCellDataAtIndex:(NSUInteger)anIndex
{
    [cellDataSource removeObjectAtIndex:anIndex];
}

- (void)removeCellData:(SMListCellData *)aCellData
{
    [cellDataSource removeObject:aCellData];
}

- (void)removeAllCellData
{
    [cellDataSource removeAllObjects];
}

- (SMListCellData *)cellDataAtIndex:(NSUInteger)anIndex
{
    return [cellDataSource objectAtIndex:anIndex];
}

- (SMListCellData *)visibleCellDataAtIndex:(NSUInteger)anIndex
{
    return [visibleCellDataSource objectAtIndex:anIndex];
}

- (NSUInteger)indexByCellData:(SMListCellData *)aCellData
{
    return [cellDataSource indexOfObject:aCellData];
}

- (NSUInteger)indexByVisibleCellData:(SMListCellData *)aCellData
{
    return [visibleCellDataSource indexOfObject:aCellData];
}

- (SMListCellData *)cellDataByTag:(NSUInteger)aTag
{
    SMListCellData *result = nil;
    
    for(SMListCellData *cd in cellDataSource)
    {
        if(cd.tag == aTag)
        {
            result = cd;
            break;
        }
    }
    
    return result;
}

- (NSUInteger)cellDataCount
{
    return [cellDataSource count];
}

- (NSUInteger)visibleCellDataCount
{
    return [visibleCellDataSource count];
}

- (void)updateCellDataVisibility
{
    [visibleCellDataSource removeAllObjects];
    for(SMListCellData *cellData in cellDataSource)
    {
        if(cellData.isVisible)
            [visibleCellDataSource addObject:cellData];
    }
}

@end
