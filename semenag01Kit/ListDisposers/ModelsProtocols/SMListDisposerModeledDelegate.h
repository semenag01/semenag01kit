//
//  SMListDisposerModeledDelegate.h
//  semenag01Kit
//
//  Created by OLEKSANDR SEMENIUK on 10/31/18.
//  Copyright © 2018 semenag01. All rights reserved.
//

#ifndef SMListDisposerModeledDelegate_h
#define SMListDisposerModeledDelegate_h

@protocol SMListDisposerModeledCreateCellDataDelegate <NSObject>

@optional
- (void)listDisposer:(SMListDisposer <SMListDisposerSetupModelProtocol> *)aListDisposer didCreateCellData:(SMListCellData *)aCellData;

@end

@protocol SMListDisposerModeledDelegate <SMListDisposerModeledCreateCellDataDelegate>

@optional
- (Class)listDisposer:(SMListDisposer <SMListDisposerSetupModelProtocol> *)aListDisposer cellDataClassForUnregisteredModel:(id)aModel;

@end


#endif /* SMListDisposerModeledDelegate_h */
