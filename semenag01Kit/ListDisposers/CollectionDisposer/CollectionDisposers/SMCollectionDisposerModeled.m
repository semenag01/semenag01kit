//
//  SMCollectionDisposerModeled.m
//  semenag01Kit
//
//  Created by OLEKSANDR SEMENIUK on 9/21/18.
//  Copyright © 2018 semenag01. All rights reserved.
//

#import "SMCollectionDisposerModeled.h"
#import "SMCollectionSection.h"
#import "SMCollectionCellData.h"

@implementation SMCollectionDisposerModeled
@synthesize modeledMulticastDelegate, modeledDelegate;

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        modeledMulticastDelegate = (SMMulticastDelegate<SMListDisposerModeledCreateCellDataDelegate> *)[SMMulticastDelegate new];
        registeredClasses = [NSMutableDictionary new];
    }
    
    return self;
}

- (void)addModeledDelegate:(id)aDelegate delegateQueue:(dispatch_queue_t)aDelegateQueue
{
    [modeledMulticastDelegate addDelegate:aDelegate delegateQueue:aDelegateQueue];
}

- (void)removeModeledDelegate:(id)aDelegate delegateQueue:(dispatch_queue_t)aDelegateQueue
{
    [modeledMulticastDelegate removeDelegate:aDelegate delegateQueue:aDelegateQueue];
}


#pragma mark - SMListDisposerSetupModelProtocol

- (void)registerCellData:(Class)aCellDataClass forModel:(Class)aModelClass
{
    if ([aCellDataClass cellNibName_] != nil)
    {
        [self.collectionView registerNib:[UINib nibWithNibName:[aCellDataClass cellNibName_] bundle:nil] forCellWithReuseIdentifier:[aCellDataClass cellIdentifier_]];
    } else
    {
        [self.collectionView registerClass:[aCellDataClass cellClass_] forCellWithReuseIdentifier:[aCellDataClass cellIdentifier_]];
    }
    
    if (aModelClass != Nil)
    {
        [registeredClasses setObject:aCellDataClass forKey:NSStringFromClass(aModelClass)];
    }
}

- (void)unregisterCellDataForModel:(Class)aModelClass
{
    [registeredClasses removeObjectForKey:NSStringFromClass(aModelClass)];
}

- (void)setupModels:(NSArray *)aModels forSectionAtIndex:(NSUInteger)aSectionIndex
{
    SMCollectionSection *section = (SMCollectionSection *)[self sectionByIndex:aSectionIndex];
    [self setupModels:aModels forSection:section];
}

- (void)setupModels:(NSArray *)aModels forSection:(SMCollectionSection *)aSection
{
    NSAssert(aSection, @"aSection is nil!!!");
    
    for(id model in aModels)
    {
        SMListCellData *cellData = [self cellDataFromModel:model];
        if(cellData)
        {
            [aSection addCellData:cellData];
            
            [self didCreateCellData:cellData];
        }
    }
}

- (SMCollectionCellData *)cellDataFromModel:(id)aModel
{
    NSString *modelClassName = NSStringFromClass([aModel class]);
    
    Class cellDataClass = [registeredClasses objectForKey:modelClassName];
    
    if(!cellDataClass && modeledDelegate && [modeledDelegate respondsToSelector:@selector(listDisposer:cellDataClassForUnregisteredModel:)])
    {
        cellDataClass = [modeledDelegate listDisposer:self cellDataClassForUnregisteredModel:aModel];
        
        [self.collectionView registerNib:[UINib nibWithNibName:[cellDataClass cellNibName_] bundle:nil] forCellWithReuseIdentifier:[cellDataClass cellIdentifier_]];
    }
    
    NSAssert(cellDataClass, (NSString *)([NSString stringWithFormat:@"Model doesn't have registered cellData class %@", NSStringFromClass([aModel class])]));
    
    SMCollectionCellData *cellData = [[cellDataClass alloc] initWithModel:aModel];
    
    return cellData;
}

- (void)didCreateCellData:(SMListCellData *)aCellData
{
    if([self.modeledDelegate respondsToSelector:@selector(listDisposer:didCreateCellData:)])
    {
        [self.modeledDelegate listDisposer:self didCreateCellData:aCellData];
    }
    
    [modeledMulticastDelegate listDisposer:self didCreateCellData:aCellData];
}

@end
