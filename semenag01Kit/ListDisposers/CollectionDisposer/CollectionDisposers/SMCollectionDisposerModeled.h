//
//  SMCollectionDisposerModeled.h
//  semenag01Kit
//
//  Created by OLEKSANDR SEMENIUK on 9/21/18.
//  Copyright © 2018 semenag01. All rights reserved.
//

#import "SMCollectionDisposer.h"
#import "SMListDisposerSetupModelProtocol.h"
#import "SMListDisposerModeledDelegate.h"


@interface SMCollectionDisposerModeled : SMCollectionDisposer <SMListDisposerSetupModelProtocol>
{
    NSMutableDictionary <NSString *, Class> *registeredClasses;
}

@end

