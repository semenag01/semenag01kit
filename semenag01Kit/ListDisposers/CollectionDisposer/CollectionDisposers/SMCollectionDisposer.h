//
//  SMCollectionDisposer.h
//  semenag01Kit
//
//  Created by OLEKSANDR SEMENIUK on 9/10/18.
//  Copyright © 2018 semenag01. All rights reserved.
//

#import "SMListDisposer.h"
#import "SMCellProtocol.h"
#import "SMMulticastDelegate.h"

@protocol SMCollectionViewDataSource <NSObject>

@optional
- (BOOL)collectionView:(UICollectionView *_Nonnull)collectionView canMoveItemAtIndexPath:(NSIndexPath *_Nonnull)indexPath;
- (void)collectionView:(UICollectionView *_Nonnull)collectionView moveItemAtIndexPath:(NSIndexPath *_Nonnull)sourceIndexPath toIndexPath:(NSIndexPath*_Nullable)destinationIndexPath;
- (nullable NSArray<NSString *> *)indexTitlesForCollectionView:(UICollectionView *_Nonnull)collectionView;
- (NSIndexPath *_Nonnull)collectionView:(UICollectionView *_Nonnull)collectionView indexPathForIndexTitle:(NSString *_Nonnull)title atIndex:(NSInteger)index;

@end


@class SMCollectionDisposer;
@protocol SMCollectionDisposerDelegate <UICollectionViewDelegateFlowLayout, SMCollectionViewDataSource, UICollectionViewDelegate>

@optional
- (void)collectionDisposer:(SMCollectionDisposer *_Nonnull)aCollectionDisposer didSetupCell:(UICollectionViewCell <SMCellProtocol>*_Nonnull)aCell at:(NSIndexPath *_Nonnull)aIndexPath;

@end


@protocol SMCollectionDisposerMulticastDelegate <NSObject>

- (void)collectionView:(UICollectionView *_Nonnull)collectionView willDisplayCell:(UICollectionViewCell *_Nonnull)cell forItemAtIndexPath:(NSIndexPath *_Nonnull)indexPath;

@end


@interface SMCollectionDisposer : SMListDisposer <UICollectionViewDelegateFlowLayout, UICollectionViewDataSource>

@property(nonatomic, readonly) UICollectionView * _Nullable collectionView;
@property(nonatomic, weak) id <SMCollectionDisposerDelegate> _Nullable delegate;
@property (nonatomic, readonly) SMMulticastDelegate<SMCollectionDisposerMulticastDelegate> * _Nullable multicastDelegate;

@end
