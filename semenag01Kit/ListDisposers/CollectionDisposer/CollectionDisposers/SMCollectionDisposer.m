//
//  SMCollectionDisposer.m
//  semenag01Kit
//
//  Created by OLEKSANDR SEMENIUK on 9/10/18.
//  Copyright © 2018 semenag01. All rights reserved.
//

#import "SMCollectionDisposer.h"
#import "SMCollectionSection.h"
#import "NSObject+Cast.h"
#import "SMCollectionCellData.h"

@implementation SMCollectionDisposer
@synthesize multicastDelegate;

- (instancetype)init
{
    self = [super init];
 
    if (self)
    {
        multicastDelegate = (SMMulticastDelegate<SMCollectionDisposerMulticastDelegate> *)[SMMulticastDelegate new];
    }
    
    return self;
}

- (void)setListView:(UICollectionView *)aListView
{
    [super setListView:aListView];
    
    aListView.delegate = self;
    aListView.dataSource = self;
}

- (UICollectionView *)collectionView
{
    return (UICollectionView *)listView;
}

- (void)didSetupCell:(UICollectionViewCell <SMCellProtocol> *)aCell at:(NSIndexPath *)aIndexPath
{
    if ([self.delegate respondsToSelector:@selector(collectionDisposer:didSetupCell:at:)])
    {
        [self.delegate collectionDisposer:self didSetupCell:aCell at:aIndexPath];
    }
}

- (UIView <SMCellProtocol> *)cellForIndexPath:(NSIndexPath *)aIndexPath
{
    return  (UIView <SMCellProtocol> *)[self.collectionView cellForItemAtIndexPath:aIndexPath];
}

- (void)reloadRows:(NSArray<NSIndexPath *> *)aIndexPathes
{
    [self.collectionView reloadItemsAtIndexPaths:aIndexPathes];
}


#pragma mark - Delegates

- (void)addDelegate:(id)aDelegate delegateQueue:(dispatch_queue_t)aDelegateQueue
{
    [multicastDelegate addDelegate:aDelegate delegateQueue:aDelegateQueue];
}

- (void)removeDelegate:(id)aDelegate delegateQueue:(dispatch_queue_t)aDelegateQueue
{
    [multicastDelegate removeDelegate:aDelegate delegateQueue:aDelegateQueue];
}


#pragma mark - Data reloading

- (void)reloadData
{
    for(SMCollectionSection *section in sections)
    {
        [section updateCellDataVisibility];
    }

    [self.collectionView reloadData];
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [sections[section] visibleCellDataCount];
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell <SMCellProtocol> *result = [[SMCollectionSection asType:sections[indexPath.section]] cellForIndexPath:indexPath];
    
    [self didSetupCell:result at:indexPath];
    
    return result;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return sections.count;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    SMCollectionSection *section = (SMCollectionSection *)sections[indexPath.section];

    UICollectionReusableView *result = nil;
    
    if ([kind isEqualToString:UICollectionElementKindSectionHeader])
    {
        result = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:section.headerViewIdentifier forIndexPath:indexPath];
        [section performHeaderSetupBlock:result];
    } else if ([kind isEqualToString:UICollectionElementKindSectionFooter])
    {
        result = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:section.footerViewIdentifier forIndexPath:indexPath];
        [section performFooterSetupBlock:result];
    }
    
    return result;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL result = NO;
    
    if ([self.delegate respondsToSelector:@selector(collectionView:canMoveItemAtIndexPath:)])
    {
        result = [self.delegate collectionView:collectionView canMoveItemAtIndexPath:indexPath];
    }
    
    return result;
}

- (void)collectionView:(UICollectionView *)collectionView moveItemAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    if ([self.delegate respondsToSelector:@selector(collectionView:moveItemAtIndexPath:toIndexPath:)])
    {
        [self.delegate collectionView:collectionView moveItemAtIndexPath:sourceIndexPath toIndexPath:destinationIndexPath];
    }
}

- (nullable NSArray<NSString *> *)indexTitlesForCollectionView:(UICollectionView *)collectionView
{
    NSArray<NSString *> *result = nil;
    
    if ([self.delegate respondsToSelector:@selector(indexTitlesForCollectionView:)])
    {
        result = [self.delegate indexTitlesForCollectionView:collectionView];
    }
    
    return result;
}

- (NSIndexPath *)collectionView:(UICollectionView *)collectionView indexPathForIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    NSIndexPath* result = nil;
    
    if ([self.delegate respondsToSelector:@selector(collectionView:indexPathForIndexTitle:atIndex:)])
    {
        result = [self.delegate collectionView:collectionView indexPathForIndexTitle:title atIndex:index];
    }
    
    return result;
}


#pragma mark - UICollectionViewDelegate

- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL result = YES;
    
    if ([self.delegate respondsToSelector:@selector(collectionView:shouldHighlightItemAtIndexPath:)])
    {
        result = [self.delegate collectionView:collectionView shouldHighlightItemAtIndexPath:indexPath];
    }
    
    return result;
}

- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(collectionView:didHighlightItemAtIndexPath:)])
    {
        [self.delegate collectionView:collectionView didHighlightItemAtIndexPath:indexPath];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(collectionView:didUnhighlightItemAtIndexPath:)])
    {
        [self.delegate collectionView:collectionView didUnhighlightItemAtIndexPath:indexPath];
    }
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    BOOL result = YES;
    
    if ([self.delegate respondsToSelector:@selector(collectionView:shouldSelectItemAtIndexPath:)])
    {
        result = [self.delegate collectionView:collectionView shouldSelectItemAtIndexPath:indexPath];
    }
    
    return result;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldDeselectItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    BOOL result = YES;
    
    if ([self.delegate respondsToSelector:@selector(collectionView:shouldDeselectItemAtIndexPath:)])
    {
        result = [self.delegate collectionView:collectionView shouldDeselectItemAtIndexPath:indexPath];
    }
    
    return result;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    SMCollectionCellData *cellData = (SMCollectionCellData *)[self cellDataByIndexPath:indexPath];
    [cellData.baSelect performBlockFromSender:cellData];
    if ([self.delegate respondsToSelector:@selector(collectionView:didSelectItemAtIndexPath:)])
    {
        [self.delegate collectionView:collectionView didSelectItemAtIndexPath:indexPath];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(collectionView:didDeselectItemAtIndexPath:)])
    {
        [self.delegate collectionView:collectionView didDeselectItemAtIndexPath:indexPath];
    }
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(nonnull UICollectionViewCell *)cell forItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(collectionView:willDisplayCell:forItemAtIndexPath:)])
    {
        [self.delegate collectionView:collectionView willDisplayCell:cell forItemAtIndexPath:indexPath];
    }
    
    [multicastDelegate collectionView:collectionView willDisplayCell:cell forItemAtIndexPath:indexPath];
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(collectionView:didEndDisplayingCell:forItemAtIndexPath:)])
    {
        [self.delegate collectionView:collectionView didEndDisplayingCell:cell forItemAtIndexPath:indexPath];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingSupplementaryView:(UICollectionReusableView *)view forElementOfKind:(NSString *)elementKind atIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(collectionView:didEndDisplayingSupplementaryView:forElementOfKind:atIndexPath:)])
    {
        [self.delegate collectionView:collectionView didEndDisplayingSupplementaryView:view forElementOfKind:elementKind atIndexPath:indexPath];
    }
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    BOOL result = NO;
    
    if ([self.delegate respondsToSelector:@selector(collectionView:shouldSelectItemAtIndexPath:)])
    {
        result = [self.delegate collectionView:collectionView shouldShowMenuForItemAtIndexPath:indexPath];
    }
    
    return result;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
    BOOL result = NO;
    
    if ([self.delegate respondsToSelector:@selector(collectionView:canPerformAction:forItemAtIndexPath:withSender:)])
    {
        result = [self.delegate collectionView:collectionView canPerformAction:action forItemAtIndexPath:indexPath withSender:sender];
    }
    
    return result;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
    [self.delegate collectionView:collectionView performAction:action forItemAtIndexPath:indexPath withSender:sender];
}

- (UICollectionViewTransitionLayout *)collectionView:(UICollectionView *)collectionView transitionLayoutForOldLayout:(UICollectionViewLayout *)fromLayout newLayout:(UICollectionViewLayout *)toLayout
{
    UICollectionViewTransitionLayout *result = nil;
    
    if ([self.delegate respondsToSelector:@selector(collectionView:transitionLayoutForOldLayout:newLayout:)])
    {
        result = [self.delegate collectionView:collectionView transitionLayoutForOldLayout:fromLayout newLayout:toLayout];
    }
    
    return result;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canFocusItemAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL result = YES;
    
    if ([self.delegate respondsToSelector:@selector(collectionView:canFocusItemAtIndexPath:)])
    {
        result = [self.delegate collectionView:collectionView canFocusItemAtIndexPath:indexPath];
    }
    
    return result;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldUpdateFocusInContext:(nonnull UICollectionViewFocusUpdateContext *)context
{
    BOOL result = YES;
    
    if ([self.delegate respondsToSelector:@selector(collectionView:shouldUpdateFocusInContext:)])
    {
        result = [self.delegate collectionView:collectionView shouldUpdateFocusInContext:context];
    }
    
    return result;
}

- (void)collectionView:(UICollectionView *)collectionView didUpdateFocusInContext:(nonnull UICollectionViewFocusUpdateContext *)context withAnimationCoordinator:(nonnull UIFocusAnimationCoordinator *)coordinator
{
    if ([self.delegate respondsToSelector:@selector(collectionView:didUpdateFocusInContext:withAnimationCoordinator:)])
    {
        [self.delegate collectionView:collectionView didUpdateFocusInContext:context withAnimationCoordinator:coordinator];
    }
}

- (NSIndexPath *)indexPathForPreferredFocusedViewInCollectionView:(UICollectionView *)collectionView
{
    NSIndexPath *result = nil;
    
    if ([self.delegate respondsToSelector:@selector(indexPathForPreferredFocusedViewInCollectionView:)])
    {
        result = [self.delegate indexPathForPreferredFocusedViewInCollectionView:collectionView];
    }
    
    return result;
}

- (NSIndexPath *)collectionView:(UICollectionView *)collectionView targetIndexPathForMoveFromItemAtIndexPath:(NSIndexPath *)originalIndexPath toProposedIndexPath:(NSIndexPath *)proposedIndexPath
{
    NSIndexPath *result = [NSIndexPath new];
    
    if ([self.delegate respondsToSelector:@selector(collectionView:targetIndexPathForMoveFromItemAtIndexPath:toProposedIndexPath:)])
    {
        result = [self.delegate collectionView:collectionView targetIndexPathForMoveFromItemAtIndexPath:originalIndexPath toProposedIndexPath:proposedIndexPath];
    }
    
    return result;
}

- (CGPoint)collectionView:(UICollectionView *)collectionView targetContentOffsetForProposedContentOffset:(CGPoint)proposedContentOffset
{
    CGPoint result = CGPointZero;
    
    if ([self.delegate respondsToSelector:@selector(collectionView:targetContentOffsetForProposedContentOffset:)])
    {
        result = [self.delegate collectionView:collectionView targetContentOffsetForProposedContentOffset:proposedContentOffset];
    }
    
    return result;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSpringLoadItemAtIndexPath:(NSIndexPath *)indexPath withContext:(id<UISpringLoadedInteractionContext>)context API_AVAILABLE(ios(11.0))
{
    BOOL result = NO;
    
    if ([self.delegate respondsToSelector:@selector(collectionView:shouldSpringLoadItemAtIndexPath:withContext:)])
    {
        result = [self.delegate collectionView:collectionView shouldSpringLoadItemAtIndexPath:indexPath withContext:context];
    }
    
    return result;
}


#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize result = CGSizeZero;

    SMCollectionCellData *cd = (SMCollectionCellData *)[self cellDataByIndexPath:indexPath];
    result = [cd cellSizeForSize:collectionView.frame.size layout:collectionViewLayout];
    
    if (CGSizeEqualToSize(result, CGSizeZero))
    {
        if (CGSizeEqualToSize(result, CGSizeZero) && [self.delegate respondsToSelector:@selector(collectionView:layout:sizeForItemAtIndexPath:)])
        {
            result = [self.delegate collectionView:collectionView layout:collectionViewLayout sizeForItemAtIndexPath:indexPath];
        } else
        {
            result = [UICollectionViewFlowLayout asType:collectionViewLayout].itemSize;
        }
    }

    return result;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    UIEdgeInsets result = UIEdgeInsetsZero;
    
    result = [SMCollectionSection asType:sections[section]].insetForSection;
    
    if (UIEdgeInsetsEqualToEdgeInsets(result, UIEdgeInsetsZero))
    {
        if ([self.delegate respondsToSelector:@selector(collectionView:layout:insetForSectionAtIndex:)])
        {
            result = [self.delegate collectionView:collectionView layout:collectionViewLayout insetForSectionAtIndex:section];
        } else
        {
            result = [UICollectionViewFlowLayout asType:collectionViewLayout].sectionInset;
        }
    }
    
    return result;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    CGFloat result = 0.0f;
    
    result = [SMCollectionSection asType:sections[section]].minimumLineSpacing;
    
    if (result == 0.0f && [self.delegate respondsToSelector:@selector(collectionView:layout:minimumLineSpacingForSectionAtIndex:)])
    {
        result = [self.delegate collectionView:collectionView layout:collectionViewLayout minimumLineSpacingForSectionAtIndex:section];
    } else
    {
        result = [UICollectionViewFlowLayout asType:collectionViewLayout].minimumLineSpacing;
    }
    
    return result;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    CGFloat result = 0.0f;
    
    result = [SMCollectionSection asType:sections[section]].minimumInteritemSpacing;
    
    if (result == 0.0f && [self.delegate respondsToSelector:@selector(collectionView:layout:minimumInteritemSpacingForSectionAtIndex:)])
    {
        result = [self.delegate collectionView:collectionView layout:collectionViewLayout minimumInteritemSpacingForSectionAtIndex:section];
    } else
    {
        result = [UICollectionViewFlowLayout asType:collectionViewLayout].minimumInteritemSpacing;
    }
    
    return result;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    CGSize result = CGSizeZero;
    
    result = [SMCollectionSection asType:sections[section]].headerReferenceSize;
    
    if (CGSizeEqualToSize(result, CGSizeZero))
    {
        if ([self.delegate respondsToSelector:@selector(collectionView:layout:referenceSizeForHeaderInSection:)])
        {
            result = [self.delegate collectionView:collectionView layout:collectionViewLayout referenceSizeForHeaderInSection:section];
        } else
        {
            result = [UICollectionViewFlowLayout asType:collectionViewLayout].headerReferenceSize;
        }
    }
    
    return result;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    CGSize result = CGSizeZero;
    
    result = [SMCollectionSection asType:sections[section]].footerReferenceSize;

    if (CGSizeEqualToSize(result, CGSizeZero))
    {
        if ([self.delegate respondsToSelector:@selector(collectionView:layout:referenceSizeForFooterInSection:)])
        {
            result = [self.delegate collectionView:collectionView layout:collectionViewLayout referenceSizeForFooterInSection:section];
        } else
        {
            result = [UICollectionViewFlowLayout asType:collectionViewLayout].footerReferenceSize;
        }
    }
    
    return result;
}


#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(scrollViewDidScroll:)])
    {
        [self.delegate scrollViewDidScroll:scrollView];
    }
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(scrollViewDidZoom:)])
    {
        [self.delegate scrollViewDidZoom:scrollView];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView;
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(scrollViewWillBeginDragging:)])
    {
        [self.delegate scrollViewWillBeginDragging:scrollView];
    }
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(scrollViewWillEndDragging:withVelocity:targetContentOffset:)])
    {
        [self.delegate scrollViewWillEndDragging:scrollView withVelocity:velocity targetContentOffset:targetContentOffset];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(scrollViewDidEndDragging:willDecelerate:)])
    {
        [self.delegate scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
    }
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(scrollViewWillBeginDecelerating:)])
    {
        [self.delegate scrollViewWillBeginDecelerating:scrollView];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(scrollViewDidEndDecelerating:)])
    {
        [self.delegate scrollViewDidEndDecelerating:scrollView];
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(scrollViewDidEndScrollingAnimation:)])
    {
        [self.delegate scrollViewDidEndScrollingAnimation:scrollView];
    }
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    UIView *view = nil;
    if(self.delegate && [self.delegate respondsToSelector:@selector(viewForZoomingInScrollView:)])
    {
        view = [self.delegate viewForZoomingInScrollView:scrollView];
    }
    return view;
}

- (void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(scrollViewWillBeginZooming:withView:)])
    {
        [self.delegate scrollViewWillBeginZooming:scrollView withView:view];
    }
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(scrollViewDidEndZooming:withView:atScale:)])
    {
        [self.delegate scrollViewDidEndZooming:scrollView withView:view atScale:scale];
    }
}

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView;   // return a yes if you want to scroll to the top. if not defined, assumes YES
{
    BOOL result = YES;
    if(self.delegate && [self.delegate respondsToSelector:@selector(scrollViewShouldScrollToTop:)])
    {
        result = [self.delegate scrollViewShouldScrollToTop:scrollView];
    }
    return result;
}

- (void)scrollViewDidScrollToTop:(UIScrollView *)scrollView
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(scrollViewDidScrollToTop:)])
    {
        [self.delegate scrollViewDidScrollToTop:scrollView];
    }
}

@end
