//
//  SMCollectionSection.h
//  semenag01Kit
//
//  Created by OLEKSANDR SEMENIUK on 9/7/18.
//  Copyright © 2018 semenag01. All rights reserved.
//

#import "SMListSection.h"
#import "SMCellProtocol.h"

typedef void (^SMCollectionSectionSetupViewBlock)(UICollectionReusableView *aCollectionReusableView);

@class SMCollectionDisposer;
@interface SMCollectionSection : SMListSection
{
    SMCollectionSectionSetupViewBlock headerSetupBlock;
    SMCollectionSectionSetupViewBlock footerSetupBlock;
    
    NSString *headerViewNibName;
    NSString *footerViewNibName;
    
    Class headerViewClass;
    Class footerViewClass;
}

@property (nonatomic, readonly) SMCollectionDisposer *collectionDisposer;

@property (nonatomic, strong) NSString *headerTitle;
@property (nonatomic, strong) NSString *footerTitle;

@property (nonatomic, readonly) NSString *headerViewIdentifier;
@property (nonatomic, readonly) NSString *footerViewIdentifier;

@property (nonatomic, assign) UIEdgeInsets insetForSection;
@property (nonatomic, assign) CGFloat minimumLineSpacing;
@property (nonatomic, assign) CGFloat minimumInteritemSpacing;
@property (nonatomic, assign) CGSize headerReferenceSize;
@property (nonatomic, assign) CGSize footerReferenceSize;

- (void)setHeaderViewNibName:(NSString *)aNibName setupBlock:(SMCollectionSectionSetupViewBlock)aSetupBlock;
- (void)setFooterViewNibName:(NSString *)aNibName setupBlock:(SMCollectionSectionSetupViewBlock)aSetupBlock;

- (void)setHeaderViewClass:(Class)aClass setupBlock:(SMCollectionSectionSetupViewBlock)aSetupBlock;
- (void)setFooterViewClass:(Class)aClass setupBlock:(SMCollectionSectionSetupViewBlock)aSetupBlock;

- (void)performHeaderSetupBlock:(UICollectionReusableView *)aView;
- (void)performFooterSetupBlock:(UICollectionReusableView *)aView;

- (UICollectionViewCell <SMCellProtocol> *)cellForIndexPath:(NSIndexPath *)aIndexPath;

@end
