//
//  SMCollectionSection.m
//  semenag01Kit
//
//  Created by OLEKSANDR SEMENIUK on 9/7/18.
//  Copyright © 2018 semenag01. All rights reserved.
//

#import "SMCollectionSection.h"
#import "SMCollectionDisposer.h"
#import "SMListCellData.h"

@implementation SMCollectionSection

- (void)setDisposer:(SMListDisposer *)aDisposer
{
    [super setDisposer:aDisposer];
    
    [self registerHeaderFooterViews];
}

- (SMCollectionDisposer *)collectionDisposer
{
    return (SMCollectionDisposer *)disposer;
}

- (void)setHeaderViewNibName:(NSString *)aNibName setupBlock:(SMCollectionSectionSetupViewBlock)aSetupBlock
{
    headerSetupBlock = aSetupBlock;
    headerViewNibName = aNibName;
}

- (void)setFooterViewNibName:(NSString *)aNibName setupBlock:(SMCollectionSectionSetupViewBlock)aSetupBlock
{
    footerSetupBlock = aSetupBlock;
    footerViewNibName = aNibName;
}

- (void)setHeaderViewClass:(Class)aClass setupBlock:(SMCollectionSectionSetupViewBlock)aSetupBlock
{
    headerSetupBlock = aSetupBlock;
    headerViewClass = aClass;
}

- (void)setFooterViewClass:(Class)aClass setupBlock:(SMCollectionSectionSetupViewBlock)aSetupBlock
{
    footerSetupBlock = aSetupBlock;
    footerViewClass = aClass;
}

- (NSString *)headerViewIdentifier
{
    return [NSString stringWithFormat:@"%@HeaderView", NSStringFromClass([self class])];
}

- (NSString *)footerViewIdentifier
{
    return [NSString stringWithFormat:@"%@FooterView", NSStringFromClass([self class])];
}

- (void)registerHeaderFooterViews
{
    if (headerViewNibName != nil)
    {
        [self.collectionDisposer.collectionView registerNib:[UINib nibWithNibName:headerViewNibName bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:self.headerViewIdentifier];
    } else if (headerViewClass != Nil)
    {
        [self.collectionDisposer.collectionView registerClass:headerViewClass forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:self.headerViewIdentifier];
    }

    if (footerViewNibName != nil)
    {
        [self.collectionDisposer.collectionView registerNib:[UINib nibWithNibName:footerViewNibName bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:self.footerViewIdentifier];
    } else if (headerViewClass != Nil)
    {
        [self.collectionDisposer.collectionView registerClass:footerViewClass forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:self.footerViewIdentifier];
    }
}

- (UICollectionViewCell <SMCellProtocol> *)cellForIndexPath:(NSIndexPath *)aIndexPath
{
    SMListCellData *cellData = (SMListCellData *)[self visibleCellDataAtIndex:aIndexPath.row];
    
    UICollectionViewCell <SMCellProtocol> *cell = (UICollectionViewCell <SMCellProtocol> *)[self.collectionDisposer.collectionView dequeueReusableCellWithReuseIdentifier:cellData.cellIdentifier forIndexPath:aIndexPath];

    [cell setupCellData:cellData];

    return cell;
}

- (void)performHeaderSetupBlock:(UICollectionReusableView *)aView
{
    if (headerSetupBlock)
    {
        headerSetupBlock(aView);
    }
}

- (void)performFooterSetupBlock:(UICollectionReusableView *)aView
{
    if (footerSetupBlock)
    {
        footerSetupBlock(aView);
    }
}

@end
