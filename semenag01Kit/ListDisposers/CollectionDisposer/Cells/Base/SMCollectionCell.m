//
//  SMCollectionCell.m
//  semenag01Kit
//
//  Created by OLEKSANDR SEMENIUK on 9/7/18.
//  Copyright © 2018 semenag01. All rights reserved.
//

#import "SMCollectionCell.h"
#import "SMListCellData.h"

@implementation SMCollectionCell
@synthesize cellData;

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];

    if (self)
    {
        [self setup];
    }

    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
  
    if (self)
    {
        [self setup];
    }
    
    return self;
}

- (instancetype)init
{
    self = [super init];

    if (self)
    {
        [self setup];
    }

    return self;
}

- (void)setup
{
    self.backgroundColor = [UIColor clearColor];
}

- (NSArray <UIResponder *>*)inputTraits
{
    return nil;
}


#pragma mark - SMCellProtocol

- (void)setupCellData:(SMListCellData *)aCellData
{
    cellData = aCellData;
    
    self.tag = aCellData.tag;
    
    __weak __typeof(&*self)weakSelf = self;
    aCellData.baPrepareForReuse = [SMBlockAction blockActionsWithBlock:^(SMListCellData *sender) {
        
        [weakSelf prepareForReuse];
    }];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.cellData.baPrepareForReuse = nil;
}

@end
