//
//  SMCollectionCell.h
//  semenag01Kit
//
//  Created by OLEKSANDR SEMENIUK on 9/7/18.
//  Copyright © 2018 semenag01. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SMCellProtocol.h"

@interface SMCollectionCell : UICollectionViewCell <SMCellProtocol>

- (void)setup;
- (NSArray <UIResponder *>*)inputTraits;

@end
