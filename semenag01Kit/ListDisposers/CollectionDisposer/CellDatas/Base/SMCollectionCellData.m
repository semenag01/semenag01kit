//
//  SMCollectionCellData.m
//  semenag01Kit
//
//  Created by OLEKSANDR SEMENIUK on 9/7/18.
//  Copyright © 2018 semenag01. All rights reserved.
//

#import "SMCollectionCellData.h"

@implementation SMCollectionCellData

- (NSString *)cellIdentifier
{
    return [[self class] cellIdentifier_];
}

+ (Class)cellClass_
{
    return nil;
}

+ (NSString *)cellNibName_
{
    return nil;
}

+ (NSString *)cellIdentifier_
{
    return NSStringFromClass(self);
}

- (CGSize)cellSizeForSize:(CGSize)aSize
{
    return self.cellSize;
}

- (CGSize)cellSizeForSize:(CGSize)aSize layout:(UICollectionViewLayout *)collectionViewLayout
{
    return [self cellSizeForSize:aSize];
}

@end
