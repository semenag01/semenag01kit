//
//  SMCollectionCellData.h
//  semenag01Kit
//
//  Created by OLEKSANDR SEMENIUK on 9/7/18.
//  Copyright © 2018 semenag01. All rights reserved.
//

#import "SMListCellData.h"

@interface SMCollectionCellData : SMListCellData

@property (nonatomic, retain) NSString *cellNibName;
@property (nonatomic, assign) Class cellClass;

@property (nonatomic, class, readonly) NSString *cellNibName_;
@property (nonatomic, class, readonly) Class cellClass_;

@property (nonatomic, class, readonly) NSString *cellIdentifier_;

@property (nonatomic, assign) CGSize cellSize;

- (CGSize)cellSizeForSize:(CGSize)aSize;

- (CGSize)cellSizeForSize:(CGSize)aSize layout:(UICollectionViewLayout *)collectionViewLayout;

@end
