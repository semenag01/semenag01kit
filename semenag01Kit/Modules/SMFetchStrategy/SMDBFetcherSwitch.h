//
//  SMDBFetcherSwitch.h
//  Semenag01Kit
//
//  Created by Developer on 1/18/18.
//  Copyright © 2018 Developer. All rights reserved.

#import "SMFetcherWithRequest.h"
#import "SMGatewayRequest.h"
#import "SMCompoundRequest.h"
#import "SMDBRequest.h"

/**
 * This fetcher automaticaly switch between requests from server to db.
 * When fether start it check if interneat connection reachable. 
 * If YES - fetcher will fetch data from server. 
 * When data was fetched, fetcher will save this data into database (insert or update)
 * If NO - fetcher will fetch data from database.
 * WARNING: Setup callbackQueue before setup request.
 **/
@interface SMDBFetcherSwitch : SMFetcherWithRequest

@property (nonatomic, assign) BOOL fetchOnlyFromDataBase;
@property (nonatomic, assign) BOOL fetchFromDataBaseWhenGatewayRequestFailed;
@property (nonatomic, assign) BOOL fetchFromDataBaseWhenGatewayRequestSuccess;
@property (nonatomic, strong) SMFetcherMessage *currentMessage;


/**
 * Override these metods
 **/

/**
 * You must return SMGatewayRequest or SMCompoundRequest
 **/
- (SMRequest *)gatewayRequestByMessage:(SMFetcherMessage *)aMessage;

- (SMDBRequest *)dataBaseRequestByMessage:(SMFetcherMessage *)aMessage;

/**
 * You can here change array of received models.
 **/
- (NSMutableArray *)processFetchedModelsAfterGatewayInResponse:(SMResponse *)aResponse;

/**
 * You can override these metods for .
 **/
- (BOOL)canFetchFromDatabaseForFailedResponse:(SMResponse *)aResponse;
- (BOOL)canFetchFromDatabaseForSuccessResponse:(SMResponse *)aResponse;

@end
