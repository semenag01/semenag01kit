//
//  SMFetcherMessagePaging.h
//  Semenag01Kit
//
//  Created by semenag01 on 29.04.13.
//  Copyright (c) 2013 semenag01. all rights reserved.
//

#import "SMFetcherMessage.h"

@interface SMFetcherMessagePaging : SMFetcherMessage

@property (nonatomic, assign) NSUInteger pagingSize;
@property (nonatomic, assign) NSUInteger pagingOffset;
@property (nonatomic, assign) BOOL isReloading;
@property (nonatomic, assign) BOOL isLoadingMore;
@property(nonatomic, strong) id cursorNext;

@end
