//
//  SMFetcherWithRequestBlock.h
//  Bittiq
//
//  Created by OLEKSANDR SEMENIUK on 10/13/17.
//  Copyright © 2017 OLEKSANDR SEMENIUK. All rights reserved.
//

#import "SMFetcherWithRequest.h"

typedef SMRequest *(^SMPrepareRequestBlock)(SMFetcherMessage *aMessage);
typedef BOOL (^SMCanFetchBlock)(SMFetcherMessage *aMessage);

@interface SMFetcherWithRequestBlock : SMFetcherWithRequest

@property (nonatomic, copy) SMPrepareRequestBlock requestBlock;
@property (nonatomic, copy) SMCanFetchBlock canFetchBlock;

- (instancetype)initWithRequestBlock:(SMPrepareRequestBlock)aRequestBlock;
- (instancetype)initWithRequestBlock:(SMPrepareRequestBlock)aRequestBlock canFetchBlock:(SMCanFetchBlock)aCanFetchBlock;

@end
