//
//  SMFetcherMessage.h
//  Semenag01Kit
//
//  Created by semenag01 on 20.04.13.
//  Copyright (c) 2013 semenag01. all rights reserved.
//

#import "SMModelObject.h"

@interface SMFetcherMessage : SMModelObject

@property (nonatomic, readonly) NSMutableDictionary *defaultParameters;

@end
