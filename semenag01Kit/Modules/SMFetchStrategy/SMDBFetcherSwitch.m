//
//  SMDBFetcherSwitch.m
//  Semenag01Kit
//
//  Created by Developer on 1/18/18.
//  Copyright © 2018 Developer. All rights reserved.
//

#import "SMDBFetcherSwitch.h"
#import "SMStorableObject.h"
#import "SMGatewayConfigurator.h"

@interface SMDBFetcherSwitch ()


@end

@implementation SMDBFetcherSwitch

#pragma mark - Request

- (void)setRequest:(SMRequest *)aRequest
{
    NSAssert(self.callbackQueue, @"SMFetcherWithRequest: callbackQueue is nil! Setup callbackQueue before setup request.");
    
    if(request == aRequest)
        return;
    
    [self cancelFetching];
    
    request = aRequest;
    
    SMWeakSelf
    [request addResponseBlock:^(SMResponse *aResponse)
     {
         if([aRequest isKindOfClass:[SMGatewayRequest class]] ||
            [aRequest isKindOfClass:[SMCompoundRequest class]])
         {
             BOOL success = aResponse.success;
             
             if(success)
             {
                 NSMutableArray *models = [weakSelf processFetchedModelsAfterGatewayInResponse:aResponse];
                 aResponse.boArray = models;
                 
                 if (weakSelf.fetchFromDataBaseWhenGatewayRequestSuccess && [weakSelf canFetchFromDatabaseForSuccessResponse:aResponse])
                 {
                     weakSelf.request = [weakSelf dataBaseRequestByMessage:weakSelf.currentMessage];
                     if (weakSelf.request)    // if no request were created we should properly return and call fetchCallback
                     {
                         [weakSelf.request start];
                     }
                     else
                     {
                         aResponse.boArray = [weakSelf processFetchedModelsInResponse:aResponse];
                         
                         if (weakSelf.fetchCallback)
                         {
                             weakSelf.fetchCallback(aResponse);
                         }
                     }
                 } else
                 {
                     aResponse.boArray = [weakSelf processFetchedModelsInResponse:aResponse];;
                     
                     if (weakSelf.fetchCallback)
                     {
                         weakSelf.fetchCallback(aResponse);
                     }
                 }
             }
             else if(weakSelf.fetchFromDataBaseWhenGatewayRequestFailed &&
                     !aResponse.requestCancelled &&
                     [weakSelf canFetchFromDatabaseForFailedResponse:aResponse])
             {
                 weakSelf.request = [weakSelf dataBaseRequestByMessage:weakSelf.currentMessage];
                 if (weakSelf.request)    // if no request were created we should properly return and call fetchCallback
                 {
                     [weakSelf.request start];
                 }
                 else
                 {
                     aResponse.boArray = [weakSelf processFetchedModelsInResponse:aResponse];
                     
                     if (weakSelf.fetchCallback)
                     {
                         weakSelf.fetchCallback(aResponse);
                     }
                 }
             }
             else
             {
                 aResponse.boArray = [weakSelf processFetchedModelsInResponse:aResponse];
                 
                 if (weakSelf.fetchCallback)
                 {
                     weakSelf.fetchCallback(aResponse);
                 }
             }
         }
         else
         {
             aResponse.boArray = [weakSelf processFetchedModelsInResponse:aResponse];
             
             if (weakSelf.fetchCallback)
             {
                 weakSelf.fetchCallback(aResponse);
             }
         }
         
     } responseQueue:self.callbackQueue];
}

- (SMRequest *)preparedRequestByMessage:(SMFetcherMessage *)aMessage
{
    if(self.currentMessage == aMessage)
    {
        return preparedRequest;
    }
    
    self.currentMessage = aMessage;
    
    SMRequest *newRequest;
    if(self.fetchOnlyFromDataBase || ![[SMGatewayConfigurator sharedInstance] isInternetReachable])
    {
        newRequest = [self dataBaseRequestByMessage:aMessage];
    }
    else
    {
        SMRequest *request = [self gatewayRequestByMessage:aMessage];
        
        if ([request isKindOfClass:[SMGatewayRequest class]] ||
            [request isKindOfClass:[SMCompoundRequest class]])
        {
            newRequest = request;
        } else
        {
            NSAssert(NO, @"%@: method \"gatewayRequestByMessage:\" should return SMGatewayRequest or SMCompoundRequest",NSStringFromClass([self class]));
        }
    }
    
    return newRequest;
}

- (SMRequest *)gatewayRequestByMessage:(SMFetcherMessage *)aMessage
{
    // override it
    return nil;
}

- (SMDBRequest *)dataBaseRequestByMessage:(SMFetcherMessage *)aMessage
{
    // override it
    return nil;
}

#pragma mark - SMDataFetcher

- (void)fetchDataByMessage:(SMFetcherMessage *)aMessage
              withCallback:(SMDataFetchCallback)aFetchCallback
{
    fetchCallback = aFetchCallback;
    
    if(!preparedRequest)
    {
        preparedRequest = [self preparedRequestByMessage:aMessage];
    }
    
    self.request = preparedRequest;
    preparedRequest = nil;
    
    [request start];
}


#pragma mark -

- (NSMutableArray *)processFetchedModelsAfterGatewayInResponse:(SMResponse *)aResponse
{
    return aResponse.boArray;
}

- (BOOL)canFetchFromDatabaseForFailedResponse:(SMResponse *)aResponse
{
    return YES;
}

- (BOOL)canFetchFromDatabaseForSuccessResponse:(SMResponse *)aResponse
{
    return YES;
}

@end
