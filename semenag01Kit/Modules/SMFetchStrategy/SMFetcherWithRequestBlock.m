//
//  SMFetcherWithRequestBlock.m
//  Bittiq
//
//  Created by OLEKSANDR SEMENIUK on 10/13/17.
//  Copyright © 2017 OLEKSANDR SEMENIUK. All rights reserved.
//

#import "SMFetcherWithRequestBlock.h"

@implementation SMFetcherWithRequestBlock

- (instancetype)initWithRequestBlock:(SMPrepareRequestBlock)aRequestBlock
{
    return [self initWithRequestBlock:aRequestBlock canFetchBlock:NULL];
}

- (instancetype)initWithRequestBlock:(SMPrepareRequestBlock)aRequestBlock canFetchBlock:(SMCanFetchBlock)aCanFetchBlock
{
    self = [super init];
    
    if (self)
    {
        self.requestBlock = aRequestBlock;
        self.canFetchBlock = aCanFetchBlock;
    }
    
    return self;
}

- (SMRequest *)preparedRequestByMessage:(SMFetcherMessage *)aMessage
{
    NSAssert(self.requestBlock != nil,@"%@ - prepareRequestBlock can't be NULL", NSStringFromClass([self class]));
    
    return self.requestBlock(aMessage);
}

- (BOOL)canFetchWithMessage:(SMFetcherMessage *)aMessage
{
    BOOL result = NO;
    
    if (self.canFetchBlock)
    {
        result = self.canFetchBlock(aMessage);
    } else
    {
        result = [super canFetchWithMessage:aMessage];
    }
    
    return result;
}

@end
