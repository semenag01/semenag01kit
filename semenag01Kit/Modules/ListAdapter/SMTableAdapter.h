//
//  SMTableAdapter.h
//  semenag01Kit
//
//  Created by OLEKSANDR SEMENIUK on 10/18/18.
//  Copyright © 2018 semenag01. All rights reserved.
//

#import "SMListAdapter.h"
#import "SMTableDisposerModeled.h"


@interface SMTableAdapter : SMListAdapter <SMTableDisposerMulticastDelegate>

@property(nonatomic, readonly) SMTableDisposerModeled *tableDisposer;

@end
