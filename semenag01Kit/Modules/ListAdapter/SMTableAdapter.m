//
//  SMTableAdapter.m
//  semenag01Kit
//
//  Created by OLEKSANDR SEMENIUK on 10/18/18.
//  Copyright © 2018 semenag01. All rights reserved.
//

#import "SMTableAdapter.h"

@implementation SMTableAdapter

- (void)dealloc
{
    [self.tableDisposer.multicastDelegate removeDelegate:self];
}

- (instancetype)initWithWith:(SMListDisposer<SMListDisposerSetupModelProtocol> *)alistDisposer
{
    self = [super initWithWith:alistDisposer];
    
    if (self)
    {
        [self.tableDisposer.multicastDelegate addDelegate:self delegateQueue:dispatch_get_main_queue()];
    }
    
    return self;
}

- (SMTableDisposerModeled *)tableDisposer
{
    return (SMTableDisposerModeled *)listDisposer;
}

- (SMListSection *)defaultSection
{
    return [SMSectionReadonly section];
}


#pragma mark - SMTableDisposerMulticastDelegate

- (void)tableDisposer:(SMTableDisposer *)aTableDisposer didCreateCell:(UITableViewCell<SMCellProtocol> *)aCell
{
    
}

- (void)tableView:(UITableView *)aTableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell conformsToProtocol:@protocol(SMPagingMoreCellProtocol)])
    {
        self.moreCell = (id <SMPagingMoreCellProtocol>)cell;
        [self.moreDelegate needLoadMoreForlistAdapter:self];
    }
}

@end
