//
//  SMListAdapter.m
//  semenag01Kit
//
//  Created by OLEKSANDR SEMENIUK on 10/18/18.
//  Copyright © 2018 semenag01. All rights reserved.
//

#import "SMListAdapter.h"
#import "SMListDisposer.h"
#import "SMListSection.h"
#import "SMListCellData.h"
#import "SMKitDefines.h"

@implementation SMListAdapter
@synthesize listDisposer;

- (instancetype)initWithWith:(SMListDisposer <SMListDisposerSetupModelProtocol> *) alistDisposer
{
    self = [super init];
    
    if (self)
    {
        listDisposer = alistDisposer;
    }
    
    return self;
}

- (SMListDisposer *)listDisposer
{
    return listDisposer;
}

- (void)reloadData
{
    [listDisposer reloadData];
}

- (void)removeAllSectionsAndCleanCells
{
    [listDisposer removeAllSectionsAndCleanCells];
}

- (void)prepareSections
{
    if (self.delegate == nil)
    {
        [listDisposer removeAllSections];
        [listDisposer addSection:[self defaultSection]];
    } else
    {
        if ([self.delegate respondsToSelector:@selector(prepareSectionsForListAdapter:)])
        {
            [self.delegate prepareSectionsForListAdapter:self];
        }
    }
}

- (SMListSection *)defaultSection
{
    return nil;
}


- (void)cleanMoreCellData
{
    for (SMListSection *section in listDisposer.sections)
    {
        NSMutableArray <SMListCellData *> *moreCellDatas = [NSMutableArray new];
        for (SMListCellData *cd in section.cellDataSource)
        {
            if ([cd conformsToProtocol:@protocol(SMPagingMoreCellDataProtocol)])
            {
                [moreCellDatas addObject:cd];
            }
        }
        
        for (SMListCellData *cd in moreCellDatas)
        {
            [section removeCellData:cd];
        }
    }
}

- (void)updateSectionWithModels:(NSArray *)aModels sectionIndex:(NSUInteger)aSectionIndex needLoadMore:(SMListAdapterClosureType)aNeedLoadMore
{
    SMListSection *section;
    
    if ([self.delegate respondsToSelector:@selector(listAdapter:sectionForModels:indexOfSection:)])
    {
        section = [self.delegate listAdapter:self sectionForModels:aModels indexOfSection:aSectionIndex];
    }
    
    if (section != nil)
    {
        [listDisposer addSection:section];
    } else if (listDisposer.sections.lastObject)
    {
        section = listDisposer.sections.lastObject;
    } else
    {
        section = [self.delegate defaultSectionForlistAdapter:self];
        
        if (section != nil)
        {
            [listDisposer addSection:section];
        }
    }
    
    NSAssert(section != nil, @"%@ section for listDisposer is nil!", NSStringFromClass([listDisposer class]));
    
    [self setupModels:aModels forSection:section];
    
    if (aNeedLoadMore != NULL && aNeedLoadMore())
    {
        SMListCellData <SMPagingMoreCellDataProtocol> *moreCellData = [self.delegate moreCellDataForListAdapter:self];
        
        if (moreCellData != nil)
        {
            Class moreCellDataType = [moreCellData class];
            [listDisposer registerCellData:moreCellDataType forModel:Nil];
            SMWeakSelf;
            moreCellData.needLoadMore = [SMBlockAction blockActionsWithBlock:^(id sender) {
                [weakSelf.moreDelegate needLoadMoreForlistAdapter:weakSelf];
            }];
            [section addCellData:moreCellData];
        }
    }
}

- (void)setupModels:(NSArray *)aModels forSection:(SMListSection *)aSection
{
    [listDisposer setupModels:aModels forSection:aSection];
}

- (void)didBeginDataLoading
{
    [self.moreCell didBeginDataLoading];
}

- (void)didEndDataLoading
{
    [self.moreCell didEndDataLoading];
}

@end
