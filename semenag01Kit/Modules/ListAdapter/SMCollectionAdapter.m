//
//  SMCollectionAdapter.m
//  semenag01Kit
//
//  Created by OLEKSANDR SEMENIUK on 10/18/18.
//  Copyright © 2018 semenag01. All rights reserved.
//

#import "SMCollectionAdapter.h"
#import "SMCollectionSection.h"

@implementation SMCollectionAdapter

- (void)dealloc
{
    [self.collectionDisposer.multicastDelegate removeDelegate:self];
}

- (instancetype)initWithWith:(SMListDisposer<SMListDisposerSetupModelProtocol> *)alistDisposer
{
    self = [super initWithWith:alistDisposer];
    
    if (self)
    {
        [self.collectionDisposer.multicastDelegate addDelegate:self delegateQueue:dispatch_get_main_queue()];
    }
    
    return self;
}

- (SMCollectionDisposerModeled *)collectionDisposer
{
    return (SMCollectionDisposerModeled *)listDisposer;
}

- (SMListSection *)defaultSection
{
    return [SMCollectionSection section];
}


#pragma mark - SMCollectionDisposerMulticastDelegate

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell conformsToProtocol:@protocol(SMPagingMoreCellProtocol)])
    {
        self.moreCell = (id <SMPagingMoreCellProtocol>)cell;
        [self.moreDelegate needLoadMoreForlistAdapter:self];
    }
}

@end
