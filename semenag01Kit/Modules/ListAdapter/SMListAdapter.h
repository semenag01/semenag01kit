//
//  SMListAdapter.h
//  semenag01Kit
//
//  Created by OLEKSANDR SEMENIUK on 10/18/18.
//  Copyright © 2018 semenag01. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SMPagingMoreCellProtocols.h"
#import "SMListDisposerSetupModelProtocol.h"

typedef BOOL (^SMListAdapterClosureType)(void);

@class SMListCellData, SMListAdapter, SMListSection;
@protocol SMListAdapterDelegate <NSObject>

@optional
- (void)prepareSectionsForListAdapter:(SMListAdapter *)aListAdapter;
- (SMListSection *)defaultSectionForlistAdapter:(SMListAdapter *)aListAdapter;
- (SMListSection *)listAdapter:(SMListAdapter *)aListAdapter sectionForModels:(NSArray *)aModels indexOfSection:(NSUInteger)aIndex;
- (SMListCellData <SMPagingMoreCellDataProtocol> *)moreCellDataForListAdapter:(SMListAdapter *)aListAdapter;

@end


@protocol SMListAdapterMoreDelegate <NSObject>

- (void)needLoadMoreForlistAdapter:(SMListAdapter *)aListAdapter;

@end


@class SMListDisposer;
@interface SMListAdapter : NSObject
{
    SMListDisposer <SMListDisposerSetupModelProtocol> *listDisposer;
}

@property(nonatomic, readonly) SMListDisposer <SMListDisposerSetupModelProtocol> *listDisposer;

@property(nonatomic, strong) id <SMPagingMoreCellProtocol> moreCell;

@property(nonatomic, weak) id <SMListAdapterMoreDelegate> moreDelegate;
@property(nonatomic, weak) id <SMListAdapterDelegate> delegate;

- (instancetype)initWithWith:(SMListDisposer <SMListDisposerSetupModelProtocol> *) alistDisposer;

- (void)reloadData;
- (void)removeAllSectionsAndCleanCells;
- (void)prepareSections;
- (SMListSection *)defaultSection;
- (void)cleanMoreCellData;

- (void)updateSectionWithModels:(NSArray *)aModels sectionIndex:(NSUInteger)aSectionIndex needLoadMore:(SMListAdapterClosureType)aNeedLoadMore;

- (void)didBeginDataLoading;
- (void)didEndDataLoading;

@end


