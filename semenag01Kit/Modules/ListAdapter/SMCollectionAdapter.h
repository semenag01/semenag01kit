//
//  SMCollectionAdapter.h
//  semenag01Kit
//
//  Created by OLEKSANDR SEMENIUK on 10/18/18.
//  Copyright © 2018 semenag01. All rights reserved.
//

#import "SMListAdapter.h"
#import "SMCollectionDisposerModeled.h"


@interface SMCollectionAdapter : SMListAdapter <SMCollectionDisposerMulticastDelegate>

@property(nonatomic, readonly) SMCollectionDisposerModeled *collectionDisposer;

@end
