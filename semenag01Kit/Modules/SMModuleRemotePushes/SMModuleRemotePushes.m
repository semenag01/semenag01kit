//
//  SMModuleRemotePushes.m
//  Semenag01Kit
//
//  Created by semenag01 on 29.04.13.
//  Copyright (c) 2013 semenag01. all rights reserved.
//

#import "SMModuleRemotePushes.h"

@implementation SMModuleRemotePushes

@synthesize deviceToken;


#pragma mark - Device token Register/Update/Unregister

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        
    }
    
    return self;
}

- (void)tryToRegisterAllNotificationSettings
{
    if (@available(iOS 10.0, *))
    {
        [self tryToRegisterAuthorizationOptions:UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge];
    } else
    {
        [self tryToRegisterForUserNotificationTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound];
    }
}

- (void)tryToRegisterForUserNotificationDefault
{
    [[UIApplication sharedApplication] registerForRemoteNotifications];
}

- (void)tryToRegisterAuthorizationOptions:(UNAuthorizationOptions)aOptions NS_AVAILABLE_IOS(10_0)
{
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    [center requestAuthorizationWithOptions:aOptions completionHandler:^(BOOL granted, NSError * _Nullable error)
     {
         if(!error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [self tryToRegisterForUserNotificationDefault];
             });
         }
     }];
}

- (void)tryToRegisterForUserNotificationTypes:(UIUserNotificationType)aUserNotificationTypes NS_DEPRECATED_IOS(8_0, 10_0)
{
    UIUserNotificationSettings* notificationSettings = [UIUserNotificationSettings settingsForTypes:aUserNotificationTypes categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
}

- (void)didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)aDeviceToken
{
    const uint64_t *tokenBytes = aDeviceToken.bytes;
    
    NSString *newDeviceToken = [NSString stringWithFormat:@"%016llx%016llx%016llx%016llx",
                                ntohll(tokenBytes[0]), ntohll(tokenBytes[1]),
                                ntohll(tokenBytes[2]), ntohll(tokenBytes[3])];
    
    SMLog(@"SMModuleRemotePushes: Device token: %@", newDeviceToken);
    
    deviceToken = newDeviceToken;
    
    [self registerForPushNotifications];
}

- (void)registerForPushNotifications
{
    if(self.deviceToken.length && [self canRegisterDeviceToken])
    {
        [[self registerDeviceTokenRequest] start];
    }
}

- (void)unregisterForPushNotifications
{
    if(self.deviceToken.length && [self canUnregisterDeviceToken])
    {
        [[self unregisterDeviceTokenRequest] start];
    }
}


#pragma mark - Process notification

- (void)receivePushNotification:(NSDictionary*)aNotificationInfo
{
    SMLog(@"SMModuleRemotePushes: Receive push notification: %@", aNotificationInfo);
}


#pragma mark - Requests

- (SMRequest*)registerDeviceTokenRequest
{
    return nil;
}

- (SMRequest*)unregisterDeviceTokenRequest
{
    return nil;
}

- (BOOL)canRegisterDeviceToken
{
    return YES;
}

- (BOOL)canUnregisterDeviceToken
{
    return YES;
}

@end
