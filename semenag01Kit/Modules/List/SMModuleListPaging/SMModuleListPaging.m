//
//  SMModuleListPaging.m
//  Semenag01Kit
//
//  Created by semenag01 on 15.04.13.
//  Copyright (c) 2013 semenag01. all rights reserved.
//

#import "SMModuleListPaging.h"
#import "SMListAdapter.h"
#import "SMFetcherMessagePaging.h"
#import "SMActivityAdapter.h"

@implementation SMModuleListPaging

@synthesize isItemsAsPage;
@synthesize initialPageOffset;
@synthesize pageSize, pageOffset;

@synthesize isLoadingMore;
@synthesize cursorEnabled, cursorNext;

- (instancetype)initWithListAdapter:(SMListAdapter *)aListAdapter
{
    self = [super initWithListAdapter:aListAdapter];
    
    if(self)
    {
        isItemsAsPage = NO;
        initialPageOffset = 0;
        
        pageSize = 20;
        self.isLoadMoreDataAuto = YES;
        pageOffset = initialPageOffset;
        
        models = [NSMutableArray new];
        
        aListAdapter.moreDelegate = self;
    }
    
    return self;
}

- (void)setDelegate:(id<SMModuleListDelegate>)aDelegate
{
    NSAssert([aDelegate conformsToProtocol:@protocol(SMModuleListPagingDelegate)],
             @"SMModuleListPaging delegate should conform SMModuleListPagingDelegate protocol");
    [super setDelegate:aDelegate];
}

- (void)clearList
{
    [super clearList];
    
    pageOffset = initialPageOffset;
}


#pragma mark - Reload

- (void)reloadData
{
    SMFetcherMessagePaging *nextMessage = (SMFetcherMessagePaging *)[self createFetcherMessage];
    
    nextMessage.pagingOffset = initialPageOffset;
    nextMessage.isReloading = YES;
    nextMessage.isLoadingMore = NO;
    nextMessage.cursorNext = nil;
    
    if([self.dataFetcher canFetchWithMessage:nextMessage])
    {
        pageOffset = nextMessage.pagingOffset;
        isReloading = nextMessage.isReloading;
        isLoadingMore = nextMessage.isLoadingMore;
        cursorNext = nextMessage.cursorNext;
        
        if([self.delegate respondsToSelector:@selector(willReloadModuleList:)])
        {
            [self.delegate willReloadModuleList:self];
        }
    }
    
    [self fetchDataWithMessage:nextMessage];
}


#pragma mark - Load more

- (void)loadMoreData
{
    if (isReloading)
    {
        return;
    }
    
    if([self.delegate respondsToSelector:@selector(willLoadMoreModuleList:)])
    {
        [(id<SMModuleListPagingDelegate>)self.delegate willLoadMoreModuleList:self];
    }
    
    if(isItemsAsPage)
    {
        pageOffset += 1;
    }
    else
    {
        pageOffset += pageSize;
    }
    
    isLoadingMore = YES;
    
    [self fetchDataWithMessage:[self createFetcherMessage]];
}

- (SMFetcherMessage *)createFetcherMessage
{
    SMFetcherMessagePaging *result = (SMFetcherMessagePaging *)[super createFetcherMessage];
    
    result.pagingOffset = pageOffset;
    result.pagingSize = pageSize;
    result.isReloading = isReloading;
    result.isLoadingMore = isLoadingMore;
    
    return result;
}

- (void)loadMoreDataPressed
{
    [self loadMoreData];
}

- (void)updateSectionWithModels:(NSArray *)aModels sectionIndex:(NSUInteger)aSectionIndex
{
    SMWeakSelf;
    
    [listAdapter updateSectionWithModels:aModels sectionIndex:aSectionIndex needLoadMore:^BOOL{
        
        BOOL result = NO;
        
        if (weakSelf)
        {
            if (weakSelf.isCursorEnabled)
            {
                result = weakSelf.cursorNext != nil;
            } else
            {
                result = (aModels.count >= weakSelf.pageSize && weakSelf.pageSize != 0);
            }
            
        }
        
        return result;
    }];
    
    [models addObjectsFromArray:aModels];
}

- (void)willFetchDataWithMessage:(SMFetcherMessagePaging *)aMessage
{
    if (isReloading)
    {
        if (!self.isHideActivityAdapterForOneFetch)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.activityAdapter showActivity:YES];
            });
        }
        
        self.isHideActivityAdapterForOneFetch = NO;
    }
    
    if(isLoadingMore)
    {
        [listAdapter didBeginDataLoading];
    }
    
    if([self.delegate respondsToSelector:@selector(willFetchDataForModuleList:)])
    {
        [self.delegate willFetchDataForModuleList:self];
    }
}

- (void)didFetchDataWithMessage:(SMFetcherMessagePaging *)aMessage andResponse:(SMResponse *)aResponse
{
    [super didFetchDataWithMessage:aMessage andResponse:aResponse];
    
    if(aResponse.success)
    {
        if (self.isCursorEnabled)
        {
            cursorNext = aResponse.cursorNext;
        }
        
        if(aMessage.isReloading)
        {
            [models removeAllObjects];
        }
        else if (aMessage.isLoadingMore)
        {
            [listAdapter didEndDataLoading];
        }
    }
    
    if (aMessage.isReloading)
    {
        isReloading = NO;
    } else if (aMessage.isLoadingMore)
    {
        isLoadingMore = NO;
    }
}


#pragma mark - Message

- (Class)fetcherMessageClass
{
    return [SMFetcherMessagePaging class];
}


#pragma mark - Sections

- (void)prepareSections
{
    if (isLoadingMore)
    {
        [listAdapter cleanMoreCellData];
    } else
    {
        [listAdapter prepareSections];
    }
}


#pragma mark - SMListAdapterDelegate

- (void)needLoadMoreForlistAdapter:(SMListAdapter *)aListAdapter
{
    if (self.isLoadMoreDataAuto)
    {
        [self loadMoreData];
    }
}

@end
