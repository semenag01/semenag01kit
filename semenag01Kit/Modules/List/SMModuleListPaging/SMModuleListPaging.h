//
//  SMModuleListPaging.h
//  Semenag01Kit
//
//  Created by semenag01 on 15.04.13.
//  Copyright (c) 2013 semenag01. all rights reserved.
//

#import "SMModuleList.h"
#import "SMPagingMoreCellProtocols.h"
#import "SMListAdapter.h"

@protocol SMModuleListPagingDelegate;


@interface SMModuleListPaging : SMModuleList <SMListAdapterMoreDelegate>

@property (nonatomic, assign) NSUInteger initialPageOffset;
@property (nonatomic, assign) BOOL isItemsAsPage;

@property (nonatomic, readonly) NSUInteger pageOffset;
@property (nonatomic, assign) NSUInteger pageSize;

@property(nonatomic, assign, getter = isCursorEnabled) BOOL cursorEnabled; // Default NO
@property(nonatomic, readonly) id cursorNext;

@property (nonatomic, readonly) BOOL isLoadingMore;
@property (nonatomic, assign) BOOL isLoadMoreDataAuto;

@end


@class SMListCellData;
@protocol SMModuleListPagingDelegate <SMModuleListDelegate>

@optional
- (SMListCellData<SMPagingMoreCellDataProtocol> *)moreCellDataForPagingModuleList:(SMModuleListPaging *)aModuleList;
- (void)willLoadMoreModuleList:(SMModuleList *)aModuleList;

@end
