//
//  SMModuleList.h
//  Semenag01Kit
//
//  Created by semenag01 on 15.04.13.
//  Copyright (c) 2013 semenag01. all rights reserved.
//


#import <Foundation/Foundation.h>

#import "SMDataFetcher.h"

@class SMModuleList;
@protocol SMModuleListDelegate;

typedef void (^SMModuleListFetcherFailedCallback) (SMModuleList* aModule, SMResponse* aResponse);
typedef void (^SMModuleListFetcherCantFetch) (SMModuleList* aModule, SMFetcherMessage *aMessage);


@class SMListAdapter, SMPullToRefreshAdapter, SMActivityAdapter;
@interface SMModuleList : NSObject
{
    SMListAdapter *listAdapter;
    
    NSDate *lastUpdateDate;
    NSMutableArray *models;
    SMPullToRefreshAdapter *pullToRefreshAdapter;
    BOOL isReloading;
}

@property (nonatomic, readonly) SMListAdapter *listAdapter; // currently active datasourced tabledisposer

@property (nonatomic, strong) SMPullToRefreshAdapter *pullToRefreshAdapter;
@property (nonatomic, strong) SMActivityAdapter *activityAdapter;
@property (nonatomic, assign) BOOL isUseActivityAdapterWithPullToRefreshAdapter;//default YES
@property (nonatomic, assign) BOOL isHideActivityAdapterForOneFetch;//this property set YES in didFetchDataWithMessage:

@property (nonatomic, strong) id<SMDataFetcher> dataFetcher;

@property (nonatomic, weak) id<SMModuleListDelegate> delegate;

@property (nonatomic, copy) SMModuleListFetcherFailedCallback fetcherFailedCallback;
@property (nonatomic, copy) SMModuleListFetcherCantFetch fetcherCantFetchCallback;

@property (nonatomic, readonly) NSDate *lastUpdateDate;
@property (nonatomic, readonly) BOOL isReloading;
@property (nonatomic, readonly) dispatch_queue_t moduleQueue;

@property (nonatomic, readonly) NSArray *models;

- (instancetype)initWithListAdapter:(SMListAdapter *)aListAdapter;

- (void)configureWithScrollView:(UIScrollView *)aScrollView;

#pragma mark - Data fetching
- (void)reloadData;

#pragma mark - For overriding
- (void)fetchDataWithMessage:(SMFetcherMessage *)aMessage;
- (void)willFetchDataWithMessage:(SMFetcherMessage *)aMessage;
- (void)didFetchDataWithMessage:(SMFetcherMessage *)aMessage andResponse:(SMResponse*)aResponse;

- (Class)fetcherMessageClass;
- (SMFetcherMessage *)createFetcherMessage;

/**
 * You can here change array of received models.
 /// * By default this method process compound models (if useCompoundCells = YES)
 **/
- (NSArray *)processFetchedModelsInResponse:(SMResponse*)aResponse;
- (void)prepareSections;
- (void)updateSectionWithModels:(NSArray *)aModels sectionIndex:(NSUInteger )aSectionIndex;

- (void)clearList;

@end

@protocol SMModuleListDelegate <NSObject>

@optional
- (SMFetcherMessage *)fetcherMessageForModuleList:(SMModuleList *)aModule;
- (void)willFetchDataForModuleList:(SMModuleList *)aModuleList;
- (void)didFetchDataForModuleList:(SMModuleList *)aModuleList;
- (void)willReloadModuleList:(SMModuleList *)aModuleList;
- (NSArray *)moduleList:(SMModuleList *)aModule processFetchedModelsInResponse:(SMResponse *)aResponse;
- (void)prepareSectionsForModuleList:(SMModuleList *)aModule;
- (void)moduleList:(SMModuleList *)aModule didReloadDataWithModels:(NSArray *)aModels;

@end

