//
//  SMModuleList.m
//  Semenag01Kit
//
//  Created by semenag01 on 15.04.13.
//  Copyright (c) 2013 semenag01. all rights reserved.
//

#import "SMModuleList.h"
#import "SMKitDefines.h"
#import "SMPullToRefreshAdapter.h"
#import "SMActivityAdapter.h"
#import "SMListAdapter.h"

@implementation SMModuleList
@synthesize listAdapter;
@synthesize delegate, lastUpdateDate;
@synthesize pullToRefreshAdapter;
@synthesize moduleQueue;
@synthesize isReloading;
@dynamic models;


#pragma mark - Init/Dealloc

- (void)dealloc
{
    [self.dataFetcher cancelFetching];
    
    moduleQueue = NULL;
}

- (instancetype)initWithListAdapter:(SMListAdapter *)aListAdapter
{
    self = [super init];
    
    if(self)
    {
        listAdapter = aListAdapter;
        moduleQueue = dispatch_queue_create("com.semenag01.SMModuleList", NULL);
        self.isUseActivityAdapterWithPullToRefreshAdapter = NO;
        models = [NSMutableArray new];
    }
    
    return self;
}


#pragma mark - Properties

- (void)setDataFetcher:(id<SMDataFetcher>)dataFetcher
{
    _dataFetcher = dataFetcher;
    _dataFetcher.callbackQueue = moduleQueue;
}

- (void)setPullToRefreshAdapter:(SMPullToRefreshAdapter *)aPullToRefreshAdapter
{
    SMWeakSelf;
    pullToRefreshAdapter = aPullToRefreshAdapter;
    pullToRefreshAdapter.refreshCallback = ^(SMPullToRefreshAdapter* aPullToRefreshAdapter)
    {
        if (!weakSelf.isUseActivityAdapterWithPullToRefreshAdapter)
        {
            weakSelf.isHideActivityAdapterForOneFetch = YES;
        }
        
        [weakSelf reloadData];
    };
}

- (NSArray *)models
{
    return [NSArray arrayWithArray:models];
}

- (void)clearList
{
    [models removeAllObjects];
    
    [self.listAdapter removeAllSectionsAndCleanCells];
    [self.listAdapter reloadData];
}


#pragma mark - Views

- (void)configureWithScrollView:(UIScrollView *)aScrollView
{
    [pullToRefreshAdapter configureWithScrollView:aScrollView];
}


#pragma mark - Data fetching

- (void)fetchDataWithMessage:(SMFetcherMessage *)aMessage
{
    if([self.dataFetcher canFetchWithMessage:aMessage])
    {
        [self.dataFetcher cancelFetching];
        [self willFetchDataWithMessage:aMessage];
        
        SMWeakSelf
        [self.dataFetcher fetchDataByMessage:aMessage withCallback:^(SMResponse *aResponse)
         {
             dispatch_sync(dispatch_get_main_queue(), ^ {
                 
                 if (aResponse.success)
                 {
                     [weakSelf prepareSections];
                 }
                 
                 [weakSelf didFetchDataWithMessage:aMessage andResponse:aResponse];
                 if(aResponse.success)
                 {
                     NSArray *aModels = [weakSelf processFetchedModelsInResponse:aResponse];
                     
                     NSUInteger numberOfPrepareSections = 0;
                     if (aModels.count)
                     {
                         for (NSInteger i = 0; i < aModels.count; i++)
                         {
                             id obj = aModels[i];
                             
                             NSArray *ms;
                             if ([obj isKindOfClass:[NSArray class]])
                             {
                                 ms = obj;
                             } else
                             {
                                 NSMutableArray *mutMs = [NSMutableArray new];
                                 
                                 for (NSInteger j = i; j < aModels.count; j++)
                                 {
                                     i = j;
                                     if (![aModels[j] isKindOfClass:[NSArray class]])
                                     {
                                         [mutMs addObject:aModels[j]];
                                     } else
                                     {
                                         i--;
                                         break;
                                     }
                                 }
                                 ms = [NSArray arrayWithArray:mutMs];
                             }
                             
                             [weakSelf updateSectionWithModels:ms sectionIndex:numberOfPrepareSections];
                             numberOfPrepareSections++;
                         }
                     } else
                     {
                         [weakSelf updateSectionWithModels:aModels sectionIndex:numberOfPrepareSections];
                         numberOfPrepareSections++;
                     }
                     
                     [weakSelf.listAdapter reloadData];
                     if([weakSelf.delegate respondsToSelector:@selector(moduleList:didReloadDataWithModels:)])
                     {
                         [weakSelf.delegate moduleList:weakSelf didReloadDataWithModels:aModels];
                     }
                 }
                 else if (!aResponse.requestCancelled)
                 {
                     if(weakSelf.fetcherFailedCallback)
                     {
                         weakSelf.fetcherFailedCallback(weakSelf, aResponse);
                     }
                 }
             });
         }];
    }
    else
    {
        isReloading = NO;
        [self.pullToRefreshAdapter endPullToRefresh];
        
        if(self.fetcherCantFetchCallback)
        {
            self.fetcherCantFetchCallback(self, aMessage);
        }
    }
}

- (void)updateSectionWithModels:(NSArray *)aModels sectionIndex:(NSUInteger )aSectionIndex
{
    [listAdapter updateSectionWithModels:aModels sectionIndex:aSectionIndex needLoadMore:nil];
    [models addObjectsFromArray:aModels];
}

- (void)willFetchDataWithMessage:(SMFetcherMessage *)aMessage
{
    if (!self.isHideActivityAdapterForOneFetch)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.activityAdapter showActivity:YES];
        });
    }
    
    if([self.delegate respondsToSelector:@selector(willFetchDataForModuleList:)])
    {
        [self.delegate willFetchDataForModuleList:self];
    }
    
    self.isHideActivityAdapterForOneFetch = NO;
}

- (void)didFetchDataWithMessage:(SMFetcherMessage *)aMessage andResponse:(SMResponse *)aResponse
{
    isReloading = NO;
    
    if(aResponse.success)
    {
        lastUpdateDate = [NSDate date];
    }
    
    if([self.delegate respondsToSelector:@selector(didFetchDataForModuleList:)])
    {
        [self.delegate didFetchDataForModuleList:self];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self.activityAdapter hideActivity:YES];
        [self.pullToRefreshAdapter endPullToRefresh];
    });
}

- (void)reloadData
{
    [models removeAllObjects];
    
    isReloading = YES;
    
    if([self.delegate respondsToSelector:@selector(willReloadModuleList:)])
    {
        [self.delegate willReloadModuleList:self];
    }
    
    [self fetchDataWithMessage:[self createFetcherMessage]];
}


#pragma mark - Message

- (Class)fetcherMessageClass
{
    return [SMFetcherMessage class];
}

- (SMFetcherMessage *)createFetcherMessage
{
    SMFetcherMessage *message = nil;
    
    if([self.delegate respondsToSelector:@selector(fetcherMessageForModuleList:)])
    {
        message = [self.delegate fetcherMessageForModuleList:self];
    }
    
    if(message)
    {
        if(![message isKindOfClass:self.fetcherMessageClass])
        {
            NSAssert(NO, @"Wrong fetcher message class!");
            return nil;
        }
    }
    else
    {
        message = [self.fetcherMessageClass new];
    }
    
    return message;
}


#pragma mark - Process Models

- (NSArray *)processFetchedModelsInResponse:(SMResponse *)aResponse
{
    NSArray *result = nil;
    if([self.delegate respondsToSelector:@selector(moduleList:processFetchedModelsInResponse:)])
    {
        result = [self.delegate moduleList:self processFetchedModelsInResponse:aResponse];
    }
    else
    {
        result = aResponse.boArray;
    }
    return result;
}

- (void)prepareSections
{
    [listAdapter prepareSections];
}

@end
