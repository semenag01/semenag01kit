//
//  SMPagingMoreCellProtocol.h
//  Semenag01Kit
//
//  Created by semenag01 on 20.04.13.
//  Copyright (c) 2013 semenag01. all rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SMPagingMoreCellProtocol <NSObject>

- (void)didBeginDataLoading;
- (void)didEndDataLoading;

@end


@class SMBlockAction;
@protocol SMPagingMoreCellDataProtocol <NSObject>

@optional
@property(nonatomic, strong) SMBlockAction *needLoadMore;

@end
