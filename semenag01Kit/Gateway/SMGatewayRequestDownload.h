//
//  SMGatewayRequestDownload.h
//  semenag01Kit
//
//  Created by OLEKSANDR SEMENIUK on 12/28/15.
//  Copyright © 2015 semenag01. All rights reserved.
//

#import "SMGatewayRequest.h"

@interface SMGatewayRequestDownload : SMGatewayRequest

@property (nonatomic,strong) NSURL *filePath;

@end
