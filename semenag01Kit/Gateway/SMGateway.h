//
//  SMGateway.h
//  Semenag01Kit
//
//  Created by semenag01 on 12.04.13.
//  Copyright (c) 2013 semenag01. all rights reserved.
//

#import <Foundation/Foundation.h>
#import "SMGatewayRequest.h"
#import "SMGatewayRequestMultipart.h"
#import "SMGatewayRequestMultipartBackground.h"
#import "SMGatewayConfigurator.h"
#import "SMResponse.h"

@class AFHTTPSessionManager;

@interface AFHTTPSessionManager()
- (NSURLSessionDataTask *)dataTaskWithHTTPMethod:(NSString *)method
                                       URLString:(NSString *)URLString
                                      parameters:(id)parameters
                                         success:(void (^)(NSURLSessionDataTask *, id))success
                                         failure:(void (^)(NSURLSessionDataTask *, NSError *))failure;
@end


@interface SMGateway : NSObject
{
    AFHTTPSessionManager *httpClient;
    NSMutableDictionary *defaultParameters;
    NSMutableArray <SMGatewayRequest *> *requests;
    __weak SMGatewayConfigurator *gatewayConfigurator;
}

@property (nonatomic, readonly) SMGatewayConfigurator *gatewayConfigurator;
@property (nonatomic, readonly) AFHTTPSessionManager *httpClient;
@property (nonatomic, assign) BOOL disableRegisterInGatewayConfigurator;

#pragma mark - Internet reachability
- (BOOL)isInternetReachable;

#pragma mark - Configuration
- (void)setupGatewayConfigurator:(SMGatewayConfigurator *)aGatewayConfigurator;

#pragma mark - Configuration
- (void)configureWithBaseURL:(NSURL*)aBaseURL;

- (NSURLSessionConfiguration *)defaultSessionConfiguration;

#pragma mark - Request generation
- (SMGatewayRequest *)requestForClass:(Class)aRequestClass
                            withType:(NSString *)aType
                                path:(NSString *)aPath
                          parameters:(NSDictionary *)aParameters;

- (SMGatewayRequest *)requestWithType:(NSString *)aType
                                path:(NSString *)aPath
                          parameters:(NSDictionary *)aParameters;

- (SMGatewayRequest *)requestWithType:(NSString *)aType
                                path:(NSString *)aPath
                          parameters:(NSDictionary *)aParameters
                        successBlock:(SMGatewayRequestSuccessBlock)aSuccessBlock
                       dispatchQueue:(dispatch_queue_t)aDispatchQueue;

- (SMGatewayRequest *)requestWithURLRequest:(NSURLRequest *)anURLRequest
                              successBlock:(SMGatewayRequestSuccessBlock)aSuccessBlock
                             dispatchQueue:(dispatch_queue_t)aDispatchQueue;

- (SMGatewayRequestMultipart *)multipartRequestWithType:(NSString *)aType
                                                  path:(NSString *)aPath
                                            parameters:(NSDictionary *)aParameters
                                          successBlock:(SMGatewayRequestSuccessBlock)aSuccessBlock
                                         dispatchQueue:(dispatch_queue_t)aDispatchQueue;

- (SMGatewayRequest *)requestWithType:(NSString *)aType
                                path:(NSString *)aPath
                          parameters:(NSDictionary *)aParameters
                  successParserBlock:(SMGatewayRequestSuccessParserBlock)aSuccessParserBlock
                       dispatchQueue:(dispatch_queue_t)aDispatchQueue;

- (SMGatewayRequestMultipartBackground *)multipartBackgroundRequestWithType:(NSString*)aType
                                                                       path:(NSString*)aPath
                                                                 parameters:(NSDictionary*)aParameters
                                                               successBlock:(SMGatewayRequestSuccessBlock)aSuccessBlock
                                                              dispatchQueue:(dispatch_queue_t)aDispatchQueue;

- (void)operationFromRequest:(SMGatewayRequest *)aRequest completionHandler:(void (^)(NSURLSessionTask *dataTask))completionHandler;

#pragma mark - Execute
- (void)startRequest:(SMGatewayRequest *)aRequest completionHandler:(void (^)(NSURLSessionTask *operation))completionHandler;
- (void)cancelAllRequests;

#pragma mark - Defaults
- (Class)defaultHTTPClientClass;
- (Class)defaultRequestClass;
- (SMGatewayRequestFailureBlock)defaultFailureBlockForRequest:(SMGatewayRequest *)aRequest;
- (void)addDefaultParameterValue:(NSString*)aValue forParameter:(NSString *)aParameter;
- (void)clearAllDefaultParameters;

#pragma mark - Retain-Release Requests
- (void)retainRequest:(SMGatewayRequest *)aRequest;
- (void)releaseRequest:(SMGatewayRequest *)aRequest;

@end
