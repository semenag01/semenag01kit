//
//  SMGatewayRequestDownload.m
//  semenag01Kit
//
//  Created by OLEKSANDR SEMENIUK on 12/28/15.
//  Copyright © 2015 semenag01. All rights reserved.
//

#import "SMGatewayRequestDownload.h"
#import "SMGateway.h"

@implementation SMGatewayRequestDownload

- (void)dataTaskWithcompletionHandler:(void (^)(NSURLSessionTask *dataTask))completionHandler
{
    SMWeakSelf;
    
    __block NSURLSessionTask *dataTask = nil;
    
    [self urlRequestWithcompletionHandler:^(NSMutableURLRequest *urlRequest, NSError *error) {
        
        assert(NO);
        dataTask = [weakSelf.gateway.httpClient downloadTaskWithRequest:urlRequest progress:^(NSProgress * _Nonnull downloadProgress) {
            
            [weakSelf executeAllDownloadProgressBlocksWith:downloadProgress];
        } destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
            
            return weakSelf.filePath;
        } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
            
            if (error)
            {    
                if(!(dataTask.state == NSURLSessionTaskStateCanceling))
                {
                    SMLog(@"\nSMGateway request failed with error:\n%@\n", error);
                }
                
                [weakSelf executeFailureBlockWithOperation:dataTask error:error];
            } else
            {
                [weakSelf executeSuccessBlockWithOperation:dataTask responseObject:filePath];
            }
        }];
        
        dataTask = [weakSelf.gateway.httpClient dataTaskWithRequest:urlRequest uploadProgress:^(NSProgress * _Nonnull uploadProgress) {
            
            [weakSelf executeAllUploadProgressBlocksWith:uploadProgress];
            
        } downloadProgress:^(NSProgress * _Nonnull downloadProgress) {
            
            [weakSelf executeAllDownloadProgressBlocksWith:downloadProgress];
            
        } completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
            
            if (error)
            {
                if(!(dataTask.state == NSURLSessionTaskStateCanceling))
                {
                    SMLog(@"\nSMGateway request failed with error:\n%@\n", error);
                }
            
                [weakSelf executeFailureBlockWithOperation:dataTask error:error];
            } else
            {
                [weakSelf executeSuccessBlockWithOperation:dataTask responseObject:responseObject];
            }
        }];
        
        if (completionHandler)
        {
            completionHandler(dataTask);
        }
    }];
}

@end
