//
//  SMGatewayRequestMultipart.h
//  Semenag01Kit
//
//  Created by semenag01 on 24.04.13.
//  Copyright (c) 2013 semenag01. all rights reserved.
//

#import "SMGatewayRequest.h"

@protocol SMMultipartFormData <AFMultipartFormData>
@end

typedef void (^SMConstructingMultipartFormDataBlock)(id<SMMultipartFormData> formData);

@interface SMGatewayRequestMultipart : SMGatewayRequest
{
    NSMutableArray *constructingBlocks;
}

- (void)addConstructingMultipartFormDataBlock:(SMConstructingMultipartFormDataBlock)block;

@end


