//
//  SMGatewayRequestMultipartBackground.m
//  Semenag01Kit
//
//  Created by semenag01 on 24.04.13.
//  Copyright (c) 2013 semenag01. all rights reserved.
//

#import "SMGatewayRequestMultipartBackground.h"
#import "SMGateway.h"
#import "SMHelper.h"

@implementation SMGatewayRequestMultipartBackground


#pragma mark - Init

- (instancetype)initWithGateway:(SMGateway*)aGateway
{
    self = [super initWithGateway:aGateway];
    
    if(self)
    {
        requestID = SMGenerateUUID();
    }
    
    return self;
}

- (NSURL *)defaultFileUrl
{
    NSURL *result = nil;
    
    NSString *documentDirectoryPath = gateway.gatewayConfigurator.defaultRequestBackgroundStoragePath;
    NSString *subPath = [NSString stringWithFormat:@"/%@",requestID];
    result = [[NSURL alloc] initFileURLWithPath:[NSString stringWithFormat:@"%@%@",documentDirectoryPath, subPath]];
    
    return result;
}


#pragma mark - Prepare request

- (void)urlRequestWithcompletionHandler:(void (^)(NSMutableURLRequest *urlRequest, NSError *error))completionHandler
{
    SMWeakSelf
    
    [super urlRequestWithcompletionHandler:^(NSMutableURLRequest *urlRequest, NSError *error) {
        
        [weakSelf.gateway.httpClient.requestSerializer requestWithMultipartFormRequest:urlRequest writingStreamContentsToFile:[weakSelf defaultFileUrl] completionHandler:^(NSError * _Nullable error) {
            
            if (completionHandler)
            {
                if (error)
                {
                    completionHandler(nil, error);
                } else
                {
                    completionHandler(urlRequest, nil);
                }
            }
        }];
    }];
}


#pragma mark - Prepare request

- (void)dataTaskWithcompletionHandler:(void (^)(NSURLSessionTask *dataTask))completionHandler
{
    SMWeakSelf;
    
    [self urlRequestWithcompletionHandler:^(NSMutableURLRequest *urlRequest, NSError *error) {
        
        __block NSURLSessionTask *dataTask = [weakSelf.gateway.httpClient uploadTaskWithRequest:urlRequest fromFile:[weakSelf defaultFileUrl] progress:^(NSProgress * _Nonnull uploadProgress) {
            
            [weakSelf executeAllUploadProgressBlocksWith:uploadProgress];
            
        } completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
            
            if (error) {
                if(!(dataTask.state == NSURLSessionTaskStateCanceling))
                    SMLog(@"\nSMGateway request failed with error:\n%@\n", error);
                [weakSelf executeFailureBlockWithOperation:dataTask error:error];
            } else {
                [weakSelf executeSuccessBlockWithOperation:dataTask responseObject:responseObject];
            }
            
            NSError *fError;
            NSString *path = [weakSelf defaultFileUrl].path;
            
            [[NSFileManager new] removeItemAtPath:path error:&fError];
            
            SMStrongSelf
            
            if (strongSelf)
            {
                [strongSelf->gateway releaseRequest:weakSelf];
            }
        }];
        
        if (completionHandler)
        {
            completionHandler(dataTask);
        }
    }];
}

@end
