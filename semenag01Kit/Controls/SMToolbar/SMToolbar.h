//
//  SMTabBar.h
//  Semenag01Kit
//
//  Created by semenag01 on 7/25/11.
//  Copyright 2011 semenag01. all rights reserved.
//

#import <Foundation/Foundation.h>

@interface SMToolbar : UIToolbar

@property (nonatomic, retain) UIImage* backgroundImage;
@property (nonatomic, assign) BOOL drawsColor;

@end
