//
//  SMLabel.h
//  BRCKS
//
//  Created by semenag01 on 5/7/15.
//  Copyright (c) 2015 brothersmedia. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SMLabel : UILabel

@property(nonatomic,assign) IBInspectable CGFloat topT;
@property(nonatomic,assign) IBInspectable CGFloat leftT;
@property(nonatomic,assign) IBInspectable CGFloat bottomT;
@property(nonatomic,assign) IBInspectable CGFloat rightT;

@end
