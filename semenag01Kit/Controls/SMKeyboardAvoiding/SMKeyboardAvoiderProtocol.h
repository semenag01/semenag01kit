//
//  SMKeyboardAvoiderProtocol.h
// Semenag01Kit
//
//  Created by semenag01 on 02.04.12.
//  Copyright (c) 2012 semenag01. all rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SMKeyboardAvoidingProtocol;

@protocol SMKeyboardAvoiderProtocol <NSObject>

@property (nonatomic, weak) id<SMKeyboardAvoidingProtocol> keyboardAvoiding;

@end
