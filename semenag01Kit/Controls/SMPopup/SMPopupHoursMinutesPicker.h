//
//  SMPopupHoursMinutesPicker.h
//  Semenag01Kit
//
//  Created by semenag01 on 12/6/11.
//  Copyright (c) 2011 semenag01. all rights reserved.
//

#import <Foundation/Foundation.h>
#import "SMPopupPicker.h"

@interface SMPopupHoursMinutesPicker : SMPopupPicker <UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, readonly) UIPickerView* popupedPicker;

@end
