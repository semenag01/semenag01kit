//
//  SMPopupDatePicker.h
//  Semenag01Kit
//
//  Created by semenag01 on 9/23/11.
//  Copyright 2011 semenag01. all rights reserved.
//

#import <Foundation/Foundation.h>
#import "SMPopupPicker.h"

@interface SMPopupDatePicker : SMPopupPicker

@property (nonatomic, readonly) UIDatePicker* popupedPicker;

//!For override
- (void)didPopupDatePickerChanged:(id)sender;

@end
