//
//  SMPopupPickerItemTitled.h
//  Semenag01Kit
//
//  Created by semenag01 on 19.10.13.
//  Copyright (c) 2013 semenag01. all rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SMPopupPickerItemTitled <NSObject>

@property (nonatomic, readonly) NSString* itemTitle;

@end
