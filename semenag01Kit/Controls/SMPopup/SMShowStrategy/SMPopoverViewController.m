//
//  SMPopoverViewController.m
//  Semenag01Kit
//
//  Created by semenag01 on 25.08.11.
//  Copyright 2011 semenag01. all rights reserved.
//

#import "SMPopoverViewController.h"
#import "SMPopupView.h"


@interface SMPopoverViewController ()

@end


@implementation SMPopoverViewController
@synthesize popupedView;

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.popupedView popupWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.popupedView popupDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.popupedView popupWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [self.popupedView popupWillDisappear:animated];
}

@end
