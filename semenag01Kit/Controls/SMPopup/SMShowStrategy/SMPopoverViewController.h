//
//  SMPopupViewController.h
//  Semenag01Kit
//
//  Created by semenag01 on 25.08.11.
//  Copyright 2011 semenag01. all rights reserved.
//

#import <UIKit/UIKit.h>

@class SMPopupView;

@interface SMPopoverViewController : UIViewController
{
}

@property (nonatomic, strong) SMPopupView *popupedView;

@end
