//
//  SMNativeMoreCollectionViewCell.m
//  semenag01Kit
//
//  Created by OLEKSANDR SEMENIUK on 10/31/18.
//  Copyright © 2018 semenag01. All rights reserved.
//

#import "SMNativeMoreCollectionViewCell.h"

@implementation SMNativeMoreCollectionViewCellData
@synthesize needLoadMore;

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        self.cellSize = CGSizeMake(45.f, 45.f);
    }
    
    return self;
}

+ (Class)cellClass_
{
    return [SMNativeMoreCollectionViewCell class];
}

- (CGSize)cellSizeForSize:(CGSize)aSize layout:(UICollectionViewLayout *)collectionViewLayout
{
    CGSize result;
    
    if ([collectionViewLayout isKindOfClass:[UICollectionViewFlowLayout class]])
    {
        switch (((UICollectionViewFlowLayout *)collectionViewLayout).scrollDirection)
        {
            case UICollectionViewScrollDirectionVertical:
            {
                result = CGSizeMake(aSize.width, self.cellSize.height);
            }
                break;

            case UICollectionViewScrollDirectionHorizontal:
            {
                result = CGSizeMake(self.cellSize.width, aSize.height);
            }
                break;

            default:
                break;
        }
    } else
    {
        result = [super cellSizeForSize:aSize];
    }
    
    return result;
}

@end


@implementation SMNativeMoreCollectionViewCell
@synthesize activity;

- (void)setup
{
    [super setup];
    
    activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    activity.hidesWhenStopped = YES;
    activity.center = CGPointMake(self.contentView.frame.size.width/2.0f, self.contentView.frame.size.height/2.0f);
    activity.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    [self.contentView addSubview:activity];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    [self showActivityIndicator:NO];
}

- (void)showActivityIndicator:(BOOL)aIsShow
{
    if (aIsShow)
    {
        [activity startAnimating];
    } else
    {
        [activity stopAnimating];
    }
}


#pragma mark - SMPagingMoreCellProtocol

- (void)didBeginDataLoading
{
    [self showActivityIndicator:YES];
}

- (void)didEndDataLoading
{
    [self showActivityIndicator:NO];
}

@end
