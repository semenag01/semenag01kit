//
//  SMNativeMoreTableViewCell.h
//  semenagKit01
//
//  Created by OLEKSANDR SEMENIUK on 9/18/17.
//  Copyright © 2017 OLEKSANDR SEMENIUK. All rights reserved.
//

#import "SMCell.h"
#import "SMPagingMoreCellProtocols.h"
#import "SMCellData.h"

@interface SMNativeMoreTableViewCellData : SMCellData <SMPagingMoreCellDataProtocol>


@end


@interface SMNativeMoreTableViewCell : SMCell <SMPagingMoreCellProtocol>
{
    UIActivityIndicatorView *activity;
}

@property(nonatomic, readonly) UIActivityIndicatorView *activity;

- (void)showActivityIndicator:(BOOL)aShow;

@end
