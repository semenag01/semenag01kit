//
//  SMNativeMoreTableViewCell.m
//  semenagKit01
//
//  Created by OLEKSANDR SEMENIUK on 9/18/17.
//  Copyright © 2017 OLEKSANDR SEMENIUK. All rights reserved.
//

#import "SMNativeMoreTableViewCell.h"

@implementation SMNativeMoreTableViewCellData
@synthesize needLoadMore;

- (instancetype)init
{
    self = [super init];
    
    if(self)
    {
        self.cellClass = [SMNativeMoreTableViewCell class];
        self.cellSelectionStyle = UITableViewCellSelectionStyleNone;
        self.cellSeparatorStyle = UITableViewCellSeparatorStyleNone;
        self.cellHeight = 45;
    }
    
    return self;
}

@end


@implementation SMNativeMoreTableViewCell
@synthesize activity;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        self.separatorInset = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, self.frame.size.width);
        self.backgroundColor = [UIColor clearColor];
        self.contentView.backgroundColor = [UIColor clearColor];
        
        activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        activity.hidesWhenStopped = YES;
        activity.center = CGPointMake(self.contentView.frame.size.width/2.0f, self.contentView.frame.size.height/2.0f);
        activity.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        
        [self.contentView addSubview:activity];
    }
    
    return self;
}

- (void)showActivityIndicator:(BOOL)aShow
{
    if(aShow)
    {
        [activity startAnimating];
    }
    else
    {
        [activity stopAnimating];
    }
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    [self showActivityIndicator:NO];
}


#pragma mark - SMPagingMoreCellProtocol

- (void)didBeginDataLoading
{
    [self showActivityIndicator:YES];
}

- (void)didEndDataLoading
{
    [self showActivityIndicator:NO];
}

@end
