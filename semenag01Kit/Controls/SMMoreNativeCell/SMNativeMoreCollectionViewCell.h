//
//  SMNativeMoreCollectionViewCell.h
//  semenag01Kit
//
//  Created by OLEKSANDR SEMENIUK on 10/31/18.
//  Copyright © 2018 semenag01. All rights reserved.
//

#import "SMCollectionCell.h"
#import "SMCollectionCellData.h"
#import "SMPagingMoreCellProtocols.h"


@interface SMNativeMoreCollectionViewCellData : SMCollectionCellData <SMPagingMoreCellDataProtocol>

@end


@interface SMNativeMoreCollectionViewCell : SMCollectionCell <SMPagingMoreCellProtocol>
{
    UIActivityIndicatorView *activity;
}

@property(nonatomic, readonly) UIActivityIndicatorView *activity;

@end
