//
//  SMAlertView.h
//  Semenag01KitDemo
//
//  Created by semenag01 on 04.09.14.
//  Copyright (c) 2014 semenag01. all rights reserved.
//

#import <UIKit/UIKit.h>


void SMShowSimpleAlertController(NSString* aTitle, NSString* aMessage);
void SMShowSimpleAlertControllerFromVc(NSString* aTitle, NSString* aMessage, UIViewController *aFromVc);


@interface SMAlertItem : NSObject

@property(nonatomic, strong) NSString *title;
@property(nonatomic, strong) UIColor *color;
@property(nonatomic, assign) UIAlertActionStyle style;

+ (instancetype)makeWithTitle:(NSString *)aTitle color:(UIColor *)aColor style:(UIAlertActionStyle)aStyle;
- (instancetype)initWithTitle:(NSString *)aTitle color:(UIColor *)aColor style:(UIAlertActionStyle)aStyle;

@end


@interface SMAlertController : UIAlertController

- (void)show;
- (void)hide;

- (void)showFromViewController:(UIViewController *)aVc;

+ (UIViewController *)topViewController;
+ (UIViewController *)topViewControllerWithRootViewController:(UIViewController *)rootViewController;

+ (instancetype)showAlertControllerWithTitle:(NSString *)title
                                     message:(NSString *)message
                          fromViewController:(UIViewController *)aVc
                           cancelButtonTitle:(NSString *)cancelButtonTitle
                           otherButtonTitles:(NSArray <id> *)otherButtonTitles
                                     handler:(void (^)(id alertController, NSInteger buttonIndex))block;

+ (instancetype)showSheetControllerWithTitle:(NSString *)title
                                     message:(NSString *)message
                          fromViewController:(UIViewController *)aVc
                           cancelButtonTitle:(NSString *)cancelButtonTitle
                           otherButtonTitles:(NSArray <id> *)otherButtonTitles
                                     handler:(void (^)(id alertController, NSInteger buttonIndex))block;
@end
