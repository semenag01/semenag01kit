//
//  SMAlertView.m
//  Semenag01KitDemo
//
//  Created by semenag01 on 04.09.14.
//  Copyright (c) 2014 semenag01. all rights reserved.
//

#import "SMAlertView.h"


void SMShowSimpleAlertController(NSString* aTitle, NSString* aMessage)
{
    SMShowSimpleAlertControllerFromVc(aTitle, aMessage, [SMAlertController topViewController]);
}

void SMShowSimpleAlertControllerFromVc(NSString* aTitle, NSString* aMessage, UIViewController *aFromVc)
{
    __block SMAlertController *ac = [SMAlertController alertControllerWithTitle:aTitle message:aMessage preferredStyle:UIAlertControllerStyleAlert];
    
    [ac addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)  style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [ac showFromViewController:aFromVc];
}


@implementation SMAlertItem

+ (instancetype)makeWithTitle:(NSString *)aTitle color:(UIColor *)aColor style:(UIAlertActionStyle)aStyle
{
    return [[self alloc] initWithTitle:aTitle color:aColor style:aStyle];
}

- (instancetype)initWithTitle:(NSString *)aTitle color:(UIColor *)aColor style:(UIAlertActionStyle)aStyle
{
    self = [self init];
    
    if (self)
    {
        self.title = aTitle;
        self.color = aColor;
        self.style = aStyle;
    }
    
    return self;
}

@end


@implementation SMAlertController

- (void)dealloc
{
    NSLog(@"DEALLOC - %@",NSStringFromClass([self class]));
}

- (instancetype)init
{
    self = [super init];
    if(self)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hide) name:UIApplicationDidEnterBackgroundNotification object:nil];
    }
    return self;
}

- (void)hide
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)show
{
    [self showFromViewController:[[self class] topViewController]];
}

- (void)showFromViewController:(UIViewController *)aVc
{
    [aVc presentViewController:self animated:YES completion:NULL];
}

+ (UIViewController *)topViewController
{
    return [self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

+ (UIViewController *)topViewControllerWithRootViewController:(UIViewController *)rootViewController
{
    if ([rootViewController isKindOfClass:[UITabBarController class]])
    {
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]])
    {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController)
    {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    } else
    {
        return rootViewController;
    }
}

+ (instancetype)showAlertControllerWithTitle:(NSString *)title
                                     message:(NSString *)message
                          fromViewController:(UIViewController *)aVc
                           cancelButtonTitle:(NSString *)cancelButtonTitle
                           otherButtonTitles:(NSArray <id> *)otherButtonTitles
                                     handler:(void (^)(id alertController, NSInteger buttonIndex))block
{
    SMAlertController *alertController = [[self class] showAlertControllerWithStyle:UIAlertControllerStyleAlert title:title message:message fromViewController:aVc cancelButtonTitle:cancelButtonTitle otherButtonTitles:otherButtonTitles handler:block];
    
    return alertController;
}

+ (instancetype)showSheetControllerWithTitle:(NSString *)title
                                     message:(NSString *)message
                          fromViewController:(UIViewController *)aVc
                           cancelButtonTitle:(NSString *)cancelButtonTitle
                           otherButtonTitles:(NSArray <id> *)otherButtonTitles
                                     handler:(void (^)(id alertController, NSInteger buttonIndex))block
{
    SMAlertController *alertController = [[self class] showAlertControllerWithStyle:UIAlertControllerStyleActionSheet title:title message:message fromViewController:aVc cancelButtonTitle:cancelButtonTitle otherButtonTitles:otherButtonTitles handler:block];
    
    return alertController;
}

+ (instancetype)showAlertControllerWithStyle:(UIAlertControllerStyle)aAlertControllerStyle
                                       title:(NSString *)title
                                     message:(NSString *)message
                          fromViewController:(UIViewController *)aVc
                           cancelButtonTitle:(NSString *)cancelButtonTitle
                           otherButtonTitles:(NSArray <id> *)otherButtonTitles
                                     handler:(void (^)(id alertController, NSInteger buttonIndex))block
{
    if (!cancelButtonTitle.length && !otherButtonTitles.count)
        cancelButtonTitle = NSLocalizedString(@"Cancel", nil);
    
    SMAlertController *alertController = [[self class] alertControllerWithTitle:title message:message preferredStyle:aAlertControllerStyle];
    
    __weak SMAlertController *__alertController;
    
    if (cancelButtonTitle.length > 0)
    {
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:cancelButtonTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            if (block)
            {
                block(__alertController,0);
            }
        }];

        [alertController addAction:cancelAction];
    }
    
    for (NSUInteger i = 0; i < otherButtonTitles.count; i++)
    {
        NSUInteger __i = i + 1;
        
        id item = otherButtonTitles[i];
        
        NSString *buttonTitle;
        UIColor *color = nil;
        UIAlertActionStyle style = UIAlertActionStyleDefault;
        
        if ([item isKindOfClass:[NSString class]])
        {
            buttonTitle = item;
        } else if ([item isKindOfClass:[SMAlertItem class]])
        {
            buttonTitle = ((SMAlertItem *)item).title;
            style = ((SMAlertItem *)item).style;
            color = ((SMAlertItem *)item).color;
        }
        
        UIAlertAction *action = [UIAlertAction actionWithTitle:buttonTitle style:style handler:^(UIAlertAction * _Nonnull action) {
            if (block)
            {
                block(__alertController,__i);
            }
        }];
        
        if (color != nil)
        {
            [action setValue:color forKey:@"titleTextColor"];
        }

        [alertController addAction:action];
    }
    
    [alertController showFromViewController:aVc];
    
    return alertController;
}

@end
