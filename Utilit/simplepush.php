<?php

// Put your device token here (without spaces):
// $deviceToken = 'e7ad217ff6ce90d5b826627d360c4c739cd5b90617480eeaa8a7a202f8878b88';
$deviceToken = 'd79a3054cc3e54dcabfe2b47bdd7776e1183703a44c13b337de3f5d21d33e2c3';//a1f5b69f23d76eb48f2f14b8b43fe1fb345dffe84121efbc2bde7057b9640dce
//$deviceToken = 'a1f5b69f23d76eb48f2f14b8b43fe1fb345dffe84121efbc2bde7057b9640dce';
// Put your private key's passphrase here:
$passphrase = '';

// Put your alert message here:
$message = array(
'body' => 'message text here',
'loc-key' => 'Вам пришло новое сообщение'
);

////////////////////////////////////////////////////////////////////////////////

$ctx = stream_context_create();
stream_context_set_option($ctx, 'ssl', 'local_cert', 'WebTaxiPush-Development.pem');
stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

// Open a connection to the APNS server
$fp = stream_socket_client(
	'ssl://gateway.sandbox.push.apple.com:2195', $err,
	$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

if (!$fp)
	exit("Failed to connect: $err $errstr" . PHP_EOL);

echo 'Connected to APNS' . PHP_EOL;

// Create the payload body
$body['aps'] = array(
	'alert' => array('loc-key' => 'broadcast_approved'),
	'badge' => 0,
	'sound' => 'broadcast_no_answer_ru.mp3',
	// 'content-available' => 1,
	);
$body['idOrder'] = '1456';
print_r($body['aps']);

// Encode the payload as JSON
$payload = json_encode($body);

// Build the binary notification
$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

// Send it to the server
$result = fwrite($fp, $msg, strlen($msg));

if (!$result)
	echo 'Message not delivered' . PHP_EOL;
else
	echo 'Message successfully delivered' . PHP_EOL;

// Close the connection to the server
fclose($fp);

//$body['aps'] = array(
//	'alert' => array('loc-key' => 'no_offers','loc-args' => array('28-01-2015 07:53' ,'Дунаевского','Кирова')),
//	'badge' => 0,
//	'sound' => 'pre_order_no_answer_ru.mp3',
//	'content-available' => 1,
//	);
//$body['idPreOrder'] = '154';
//$body['aps'] = array(
//	'alert' => array('loc-key' => 'broadcast_not_processed','loc-args' => array('28-01-2015 07:53' ,'Дунаевского')),
//	'badge' => 0,
//	'sound' => 'broadcast_no_answer_ru.mp3',
//	'content-available' => 1,
//	);
//$body['idPreOrder'] = '1382';
